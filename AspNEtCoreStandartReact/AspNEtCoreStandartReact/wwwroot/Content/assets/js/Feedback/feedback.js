$(document).ready(function () {

    var lang = $('html').attr('lang');


    var l_ro = {
        p_init: {},
        leave_feedback: "Lăsați un feedback",
        rate_us: "Evaluare:",
        comment_placeholder: '<input id="g-text" placeholder="Mesajul Dvs." class="message-txt" type="text" name="g-text" autofocus="autofocus">',
        send_cta: "Trimite",
        sent_title: "Mesajul a fost trimis",
        thanks_msg: "Multumim pentru feedback-ul dvs!",
        conditions_text: "*Limita zilnică este de 3 mesaje.",
        ok_cta: "OK",
        cancel_cta: "Anulare",
        lang_title: "",
    }

    var l_ru = {
        p_init: {},
        leave_feedback: "Оставить отзыв",
        comment_placeholder: '<input id="g-text" placeholder="Ваш коментарий" class="message-txt" type="text" name="g-text" autofocus="autofocus">',
        send_cta: "Отправить",
        rate_us: "Оценка:",
        sent_title: "Сообщение отправлено",
        thanks_msg: "Спасибо за Ваш отзыв!",
        conditions_text: "* До 3-х сообщений в день.",
        ok_cta: "OK",
        cancel_cta: "Отмена",
        lang_title: "",
    }

    var l_en = {
        p_init: {},
        leave_feedback: "Leave feedback",
        rate_us: "Rate us:",
        comment_placeholder: '<input id="g-text" placeholder="Your message..." class="message-txt" type="text" name="g-text" autofocus="autofocus">',
        send_cta: "Send",
        sent_title: "Message was sent",
        thanks_msg: "Thank you for your feedback!",
        conditions_text: "*Daily limit is 3 messages.",
        ok_cta: "OK",
        cancel_cta: "Cancel",
        lang_title: "",
    }
    var lngt = l_ro;
    function apply_lang(lang) {
        //var default_language = 'ro';
        //console.log(lang)

        switch (lang) {
            case 'ro': var lngt = l_ro; break;
            case 'ru': var lngt = l_ru; break;
            case 'en': var lngt = l_en; break;
            case 'fr': var lngt = l_fr; break;
            default: var lngt = l_ro; break;
        }

        //console.log('lng: '+lngt.leave_feedback);

        $('*[data-lang]').each(function () {
            var el = $(this), el_p = el.attr("data-lang")
            el.html(lngt[el_p]);
        });
    }
    setTimeout(function () {
        apply_lang(lang);
    }, 300)

    var stylesheet = '/Content/assets/css/feedback.css';
    var tmpl = '<div id="feeback-box">' +

        '<div class="feeback-icon">' +
        '<div class="inner">' +
        '<img alt="Feedback" title="Feedback" src="data:image/gif;base64,R0lGODlhPADCAPMPAP9mAJ9BB3EvDDIaEPRiAV4pDZE7Cd5ZArpLBeleAkoiDq1GBsZQBIE2ChEREdJVAyH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjFFQ0E0QjRFNzNDNDExRTk4QkQ2RTk1Q0YwQkE5QUQ4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjFFQ0E0QjRGNzNDNDExRTk4QkQ2RTk1Q0YwQkE5QUQ4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NUQyNDdDNzU3M0MzMTFFOThCRDZFOTVDRjBCQTlBRDgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NUQyNDdDNzY3M0MzMTFFOThCRDZFOTVDRjBCQTlBRDgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAsAAAAADwAwgAABP/QyUmrvTjrzbv/YCiOZGmeaKqubOu+cCzPdG3feK7vfO+bBdAgkAsgPAHCowg4ahoJAGCJC0gfg0vhIb0ypYfsZIDodqk3azcsSZrPuQLBnDBE3wBCQIxTHPB4CAo9AwyAV4M/Dgt4DwKKFAZvBpAVcmaUOFyHnG9oMJudop8voaKcpC6mp4CpLQsPsbKztLWxC5W5uru8KQUBpA8BiT0Ff1IVXQt8cXNdyWvMN3fPFG8HOpJv0Jg5hl3DFQMGzgDYONREGAVm0jPtGscAjzdmG6H0NmbEF9T5NaEYZNAmxZ0MRmeCUBhCJ4eCVrGogdOhhlUCgzTKnCKgcAe5Tgz/MOIwgECekgX8eqlcqUGAQB0KEEhEpeMSqyk6Vo3KIeAmHBwawdxR8oAaAwMpachTdyeRgDkcdcBzoCaTA20Ekr6r5qAnAHUSnDmpx9Xrp1A5qFmVQoCCvH80vuVB6cCZOnZd4M4gKIWI3AOrOtYYUO7rVU4JdHjtK2HmxJqb7haWMnaHAgP5yNw5YJWlZ0W/EMhCEEBwDyiIO+sI2gnLDtai2OBYfBOsDblSGAQQwJvkm7Y4ykW14MeM6a1SVFN4mBeHPQ34cMi710Xki4p6x/y8MeCYbHEah+PoLiVB6Qm/7iQ4rsrnIVcsdLrHKUO+e/gr7PvErwKWrf+24PLZ/4AEFsiCAQEkqOCCDDaYoHIt6HcTfylIyAqFKFh4CoYnaLiTDAXwJqIAqIGxG28B/CUAezNsQkB2xniRA0G2WfLYDaFYdwxwZCEDXRcswmCcBuVkF0M5L11Q0Tw44HZAAxXEtM1sED0wWRM64CYKAdbBMICH4lF0pRkh/TCOaGY8QJeBioi4SwEyPfeAID4MgNCUXBAA4WDy4NnFnjNoKWcXWcXhSVAS4CbgDXe+2BVXQSUm3WOLadeFVjFMVekE0fUIwASbStCpPtVJEGpdzd2wlKllPcdoN5vaBECSNjC3RlDATKLDnRvuQN5GmNJgpyjr/QCnYw8A6quIQbKpGNpvb8ZZzZzBDsbrtHkoK8OveIj65w6CYstWtS/gdQaiDiiaQ6OPhBppDqs+6qMDA+zjXKnyfsppqqTOe+qoNWjKFapSGAlDvKHSti6s1chKaw22goHrKtq+cG1rvvbJSaG+XvxGsT4c20jF4zHrLA2h1FhJyhUgmKDBKN/oLWM8sEyBzTvgPLNhNcvsgM45AA20Jj4PjWPRPhNN875L54R000E/zXPOZzBIjTAOBkCyCR5iXN98jbQItidij70dDC5nrfaCW5/s9ttwxy333HTXbffdeOetdy8RAAA7" class="fi-dtop">' +
        '<img alt="Feedback" title="Feedback" src="data:image/gif;base64,R0lGODlhwgA8APMPAP9mANJVA3EvDPRiATIaEN5ZArpLBYE2CuleApE7CUoiDq1GBl4pDcZQBBEREZ9BByH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjFFQ0E0QjU2NzNDNDExRTk4QkQ2RTk1Q0YwQkE5QUQ4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjFFQ0E0QjU3NzNDNDExRTk4QkQ2RTk1Q0YwQkE5QUQ4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MUVDQTRCNTQ3M0M0MTFFOThCRDZFOTVDRjBCQTlBRDgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MUVDQTRCNTU3M0M0MTFFOThCRDZFOTVDRjBCQTlBRDgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAsAAAAAMIAPABABP/QyUmrvTjrzbv/YCiOZGmeaKqubOu+cCxXD2ADT3ggdz/kmF3v9sM8BkOcoAeUBHoOQ3KQuBCeScCPcBHwsgZuBWujEAq9glhWuzU1y6wcIHDE58l6L9BlUsgAfTd8EgRSeGUSeyCASQczFm02bxwHSHMGF5Z4mRUJcggHfhOAn1kDDGZoWQmSAAoUpnKoFoAVhzYIkLu8vb6/wMHCw8TFxsfIycrLHK6Iicl3OH9QzLbFznhNrp0Trts93RLfEqJEaw7Ob4C0E7I2sA7mNuIOX9DSCOgNo05QDJe0pOpFrsMqLejGPVjY5OCAhOkYAsE1zUzAiv4GVSBwsY6DRRT/FAyZQLEKhXkAFlC7MeEegHrMYsqcSbOmzZs4c+rcybOnz59AgwpN92ykMmnrqi27RqxgTqQroSljOsxps6KUsmV5Iw1R0qIABkqgOIdr0QZjlLrSx0urnCY9VFpYoDTuBbosHQCCiHdSVDqR7MIhS0opBQGIBYjVm6YfQccZpBlITLkyF8mVMwvg0kOXhXdf4VlAiZbooMVM0zAaYsAlpRlWNXD00WDhQRseHcwmUvvBbcASftde4NJv4SEFFhZv4hLBggf8klC4PcDAA+Igj0sFRAinAIYJ4nH4vjD8eIaLPxA4gF6DAoa5Oyhgv/AAxKH48+vfz7+///8ABijg/4AEFmjggQgmqOCCDDbo4IMQRuidRBRWuNAy78EXi0QxtcKQMW7NsQxU2gVijWHCrBXAiiy2uOKIjlGVjIzBxObBAQuwuMAjleS44o4bEADdig/AQmJGiQjgY233TfCeATom0OQE7LFonXhp5TUBAZVhCYONGpCVBExRIAJTX0kAEtpLc5i05UVZqOEJInL+RQFFdcK2h4UaSqDARQEcIMAB7Ij35x6CEuqDeM09MGh0MQ7xA2JkdUcAa4gdACkABUzXWQKPDoHlNe+0s0uISTR0g2cVHNRpcKte4KoEaLrZUqQ3yOWpGxIk0GJ8Dtw2AZo8TsBAD6UhCcBuAv2iou+LLZrU1TOpTItIKgG9WgFKa2qSnZ9DiqjIDdp20Mi3bUGGgbV41MHuHHpoZMGRjGl5WHa/ARCAAb7WJa8H53bmrLoYIPuBwQb1ABFFa9pK6ygHsTqBsMpe0KKu9dLjQKlTugDmBa7kuXF2IaPzDiHSmErAphhlbNxYPkzgwxoK/DZBymKd0QNqSnWVXgwfX/DOHBiP/AzGx85x0ZpwdrYGqhRL8C4RXlLVVbFsECybAU0XgPVGXCfh9brFBaAAvbYkAGcAP18xRCgOJG1DuXEHbICXFYc0xGsS9u3334AHLvjghBdu+OGIJ6744rxEAAA7" class="fi-mobile">' +
        '</div>' +
        '</div>' +

        '<div class="feeback-contents">' +

        '<div class="inner">' +


        '<!--step 1: form-------------------------------------->' +
        '<div class="flow-step active">' +
        '<div class="popup-close-icon" data-scope="close-flow">&times;</div>' +
        '<h2 data-lang="leave_feedback"></h2> ' +
        '<div class="rate-overlay">' +

        '<center>' +
        '<small data-lang="rate_us"></small>' +
        '</center>' +

        '<div id="stars-rate">' +
        '<span>★</span>' +
        '<span>★</span>' +
        '<span>★</span>' +
        '<span>★</span>' +
        '<span>★</span>' +
        '</div>' +
        '</div>' +
        '<span data-lang="comment_placeholder"></span>' +
        '<div style="font-size:12px; color:#777; padding:10px 0; float:left;" data-lang="conditions_text"></div>' +
        '<div class="message-send noselect" data-lang="send_cta"></div>' +
        //'<br />'+
        //'<div class="simple-button" data-scope="close-flow" data-lang="cancel_cta"></div>'
        '</div>' +


        '<!--step 2: positive-------------------------------------->' +
        '<div class="flow-step" data-g-scope="success"]>' +
        '<div class="popup-close-icon" data-scope="close-flow">&times;</div>' +
        '<h2 data-lang="sent_title"></h2> ' +
        '<div class="rate-overlay theme-success" data-lang="thanks_msg"></div>' +
        '<br />' +
        '<div class="step-button-cta" data-scope="close-flow" data-lang="ok_cta"></div>'

    '</div>' +

        /**/
        //	'<!--step 3: negative-------------------------------------->'+
        //	'<h2>Oops</h2> '+
        //	'<div class="rate-overlay theme-warn">'+
        //		'Your message wasn\'t sent<br /> Daily limit was exceeded.' +
        //		'</div>'+
        //	'<div class="step-button-cta" data-scope="close-flow">OK</div>'
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';



    $("head").append('<link rel="stylesheet" type="text/css" href="' + stylesheet + '">');
    $("body").append(tmpl);



    function reset_flow() {
        $('#feeback-box .feeback-contents').removeClass('active');
        $('#feeback-box .flow-step').removeAttr('style');
        $('#feeback-box .flow-step.active').removeClass('active');
        $('#feeback-box .flow-step:first').addClass('active');
        $('#stars-rate').removeClass('rated');
        $('#stars-rate > span').removeClass('active')
        $('#g-text').val("");
    }
    function send_greeting(message, votes, page_title, site_url) {
        //console.log('sending...')
        if (message.length > 1) {
            var msisdn = " ";
            //debugger;
            if ($('#userMsisdn').length) {
                msisdn += document.getElementById('userMsisdn').innerHTML;
            }
            $('#feeback-box .flow-step.active').hide();
            $('#feeback-box .flow-step[data-g-scope="success"]').fadeIn();
            $.ajax({
                url: 'https://cdn.orange.md/myorange/feedback/save.php?n=' + encodeURIComponent(site_url) + '&m=' + encodeURIComponent(message + msisdn) + '&v=' + encodeURIComponent(votes) + '&p=' + encodeURIComponent(page_title),
                method: 'GET',
            }).done(function (e, x, y) {

                //$('.flow-step.active').hide();

                if (e.indexOf('record was added') >= 0) {
                    console.log('RECORD ADDED');

                    $('.flow-step[data-g-scope="success"]').fadeIn();
                } else {
                    console.log('OOPS!');
                    $('.flow-step[data-g-scope="oops"]').fadeIn();
                }
            });
        } else {
            $('#g-text').attr('style', 'border-bottom:1px #f00 solid !important; padding-left: 5px');
        }
    }
    //Events:


    $('body').on('mouseenter', '#stars-rate > span', function () {

        if (!$('#stars-rate').hasClass('rated')) {
            var el = $(this);
            //console.log(el.index());
            $('#stars-rate > span.active').removeClass('active');
            for (var i = -1; i < el.index(); i++) {
                $('#stars-rate > span:eq(' + (i + 1) + ')').addClass('active');
            }
        }

    });


    $('body').on('click', '#stars-rate > span', function () {
        $('#stars-rate').addClass('rated');
    });
    /////////////////////////////////////////////////////////////////////////
    $('body').on('click', '#feeback-box .message-send', function () {
        var text = $('#g-text').val();
        var votes = $('#stars-rate > span.active').length;
        var page_title = $('title').text();
        var site_url = window.location.href;
        site_url = site_url.replace("https://", "");
        site_url = site_url.replace("http://", "");
        //console.log(page_title);

        send_greeting(text, votes, page_title, site_url);

    });
    /////////////////////////////////////////////////////////////////////////
    $("body").on('click', '#feeback-box .feeback-icon', function () {
        $('.feeback-contents').addClass('active');
    });
    /////////////////////////////////////////////////////////////////////////
    var mouse_is_inside = false;

    $('#feeback-box .feeback-contents > .inner').hover(function () {
        mouse_is_inside = true;
    }, function () {
        mouse_is_inside = false;
    });

    $("body").mouseup(function () {
        if (!mouse_is_inside) {
            reset_flow();

        };
    });
    /////////////////////////////////////////////////////////////////////////
    $("body").on('click', '#feeback-box *[data-scope="close-flow"]', function () {
        reset_flow();
    });

    /////////////////////////////////////////////////////////////////////////
    $('body').on('keyup', function (e) {

        if (e.which == 27) {
            // Close my modal window
            reset_flow();
        }

        if (e.keyCode == 13) {
            // Do something
            if ($('#feeback-box .message-send').is(':visible')) {
                $('#feeback-box .message-send').trigger('click');
            } else {
                reset_flow();
            }
        }
    });




});