﻿$("body").on("click", "*[data-invoice]", function (e) {
    e.preventDefault;
    "use strict";
    var el = $(this),
        el_ajax_url = el.attr("data-invoice"),
        el_modal_tmpl = el.attr("data-popup-tmpl");
    open_popup_box_invoice(el_ajax_url, el_modal_tmpl, el);
});
function open_popup_box_invoice(url, tmpl, sender) {
    "use strict";
    if (!tmpl) {
        tmpl = "default";
    }
    var tmpl_contents = tmpl;

    if (url == "#") {
        if (tmpl.indexOf("invoice_") == 0) {
            var $dialog = $('#' + tmpl_contents);
            if (tmpl.indexOf("invoice_details_template") == 0) {
                tmpl = "factura";
            }
            else
            {
                tmpl = "short";
            }

            $.each(sender.get(0).attributes, function (i, attrib) {
                console.log('attrib.name=' + attrib.name + '; value=' + attrib.value);

                if (attrib.name == 'data-invoice-id' || attrib.name == 'data-invoice-has-email' ||
                    attrib.name == 'data-invoice-hasserviceinvoiceactive' || attrib.name == 'data-invoice-user-id' ||
                    attrib.name == 'data-invoice-included' || attrib.name == 'data-invoice-extraoptions' ||
                    attrib.name == 'data-invoice-total-amount' || attrib.name == 'data-invoice-isb2b') {
                    $('#sendInvoiceByEmailFromDetailsPopup, #btn-dialog-send-by-eml-confirmation').attr(attrib.name, attrib.value);
                    //  $(').attr(attrib.name, attrib.value);
                }
                else if (attrib.name == 'data-dialog-ico') {
                    $dialog.find('.' + attrib.name).attr('class', attrib.value);
                }
                else {
                    $dialog.find('.' + attrib.name).html(attrib.value);
                }
            });

            if (sender.attr("data-invoice-included")) {
                var extraoptions = JSON.parse(sender.attr("data-invoice-included"));
                var fullContent = "";
                ///debugger;
                for (var tempItem in extraoptions) {
                    if (extraoptions[tempItem].optionName != null && $.isNumeric(tempItem)) {
                        fullContent += "<div class='f-item'><div class='flex'>" +
                            "<div class='w-50'><b class='txt-m-size'>" +
                            extraoptions[tempItem].optionName +
                            "</b></div>" +
                            "<div class='w-25'><span class='txt-m-size txt-color-digital_dark_grey_1'><center>" +
                            extraoptions[tempItem].optionQuantity +
                            "</center></span></div>" +
                            "<div class='w-25'><span class='txt-m-size txt-color-cta'>" +
                            extraoptions[tempItem].optionPrice + "</span></div>" +
                        "</div></div>";
                    }
                }
                $dialog[0].children["invoicesIncludedServicesListContainer"].innerHTML = fullContent;
            }

            if (sender.attr("data-invoice-extraoptions")) {
                var extraoptions1 = JSON.parse(sender.attr("data-invoice-extraoptions"));
                var fullContent1 = "";
                ///debugger;
                for (var tempItem1 in extraoptions1) {
                    if (extraoptions1[tempItem1].optionName !== null && $.isNumeric(tempItem1)) {
                        fullContent1 += "<div class='f-item'><div class='flex'>" +
                            "<div class='w-50'><b class='txt-m-size'>" +
                            extraoptions1[tempItem1].optionName +
                            "</b></div>" +
                            "<div class='w-25'><span class='txt-m-size txt-color-digital_dark_grey_1'><center>" +
                            extraoptions1[tempItem1].optionQuantity +
                            "</center></span></div>" +
                            "<div class='w-25'><span class='txt-m-size txt-color-cta'>" +
                            extraoptions1[tempItem1].optionPrice +
                            "</span></div>" +
                        "</div></div>";
                    }
                }
                $dialog[0].children["invoicesExtraServicesListContainer"].innerHTML = fullContent1;
            }

            if (sender.attr("data-invoice-total-amount"))
            {
                var tempamount = sender.attr("data-invoice-total-amount");
                $dialog[0].children[19].children[0].children[2].children["total_invoice_anount_details"].innerHTML = tempamount;
            }
            var data = $dialog.html();
            create_popup(data, tmpl);
        }
    } else {
        $.ajax({
            url: url,
            data: { "tmpl": tmpl },
        })
            .done(function (data) {
                create_popup(data, tmpl);
            });
    }
}

function open_popup_box_invoice_MesageWithLink(url, tmpl, sender) {
    "use strict";
    if (!tmpl) {
        tmpl = "default";
    }
    var tmpl_contents = tmpl;
    if (url == "#") {
        if (tmpl.indexOf("local_") == 0) {
            var $dialog = $('#' + tmpl_contents);
            tmpl = "short";
            if (sender.attr("data-dialog-message"))
            {
                var containerId = sender.attr("data-dialog-message");
                $dialog[0].children[0].children[2].appendChild(document.getElementById(containerId));//.innerHTML = document.getElementById(containerId);
            }
            var data = $dialog.html();
            create_popup(data, tmpl);
        }
    } else {
        $.ajax({
            url: url,
            data: { "tmpl": tmpl },
        })
            .done(function (data) {
                create_popup(data, tmpl);
            });
    }
}

function showResultDialogInvoiceMesageWithLink(data) {
    if (!data.isSuccess) {
        var el = $('<h1></h1>');
        el.attr('data-dialog-message', data.messageContainerWithLink);
        open_popup_box_invoice_MesageWithLink("#", "local_message_dialog_withlink", el);
    }
}

function showResultDialogInvoice(data) {
    if (data.isSuccess) {
        var el = $('<h1></h1>');
        el.attr('data-dialog-message', data.email);
        open_popup_box_invoice("#", "invoice_message_dialog_InvoiceWasSended", el);
    }
    if (data.messageContainerWithLink == 'messageWithLinkContainer')
    {
        showResultDialogInvoiceMesageWithLink(data);
    }else {
        showResultDialog(data);
    }
}


function ActivateInvoicesByEmail(invoiceList)
{
    var modelVerify = invoiceList;
    var text = $('#activateInvoiceByEmail').text();
    OMDAPI.util.jsonPost('/Invoices/SendInvoiceByItems', invoiceList, function (data) {
        ///console.info(data);
        close_popup_box(true);
        showResultDialogInvoice(data);
    }, function () { },
        "activateInvoiceByEmail");
}

$('body').on('click', '#btn-dialog-send-by-eml-confirmation', function (e) {

    var invoiceList = { invoiceID: $(this).attr('data-invoice-id'), UserID: $(this).attr('data-invoice-user-id') };
    console.log(invoices);
    OMDAPI.util.jsonPost('/Invoices/SendInvoiceByItems', invoiceList, function (data) {
        console.info(data);
        close_popup_box(true);
        showResultDialogInvoice(data);
    }, function () { },
        "btn-dialog-send-by-eml-confirmation");
});

$('body').on('click', "#sendInvoiceByEmailFromDetailsPopup", function (e)
{
    var model = { Id: $(this).attr('data-invoice-id') };
    var data = { isSuccess: true, message: "Factura " + model.Id + " a fost Transmisa cu succes" };
    var hasEmail = $(this).attr('data-invoice-has-email');
    var hasServiceInvoice = $(this).attr('data-invoice-hasserviceinvoiceactive');
    var isB2B = $(this).attr('data-invoice-isb2b');

    if (hasEmail == "True") {
        if (hasServiceInvoice == "True") {
            var text = $('#sendInvoiceByEmailFromDetailsPopup').text();
            var invoiceList = { invoiceID: $(this).attr('data-invoice-id'), UserID: $(this).attr('data-invoice-user-id') };
            OMDAPI.util.jsonPost('/Invoices/SendInvoiceByItems', invoiceList, function (data) {
                console.info(data);
                close_popup_box(true);
                showResultDialogInvoice(data);
            }, function () { },
                "sendInvoiceByEmailFromDetailsPopup");
        }
        else
        {
            close_popup_box(true);
            if (isB2B =="True") {
                showResultDialog({ isSuccess: false, message: document.getElementById('no_have_email_setted_message').value });
            }
            else {
                showResultDialogInvoiceMesageWithLink({ "isSuccess": false, "messageContainerWithLink": "messageWithLinkContainer" })
            }
        }
    } else
    {
        close_popup_box(true);
        if (isB2B =="True") {
            showResultDialog({ isSuccess: false, message: document.getElementById('no_have_email_setted_message').value });
        }
        else
        {
            showResultDialogInvoiceMesageWithLink({ "isSuccess": false, "messageContainerWithLink": "messageWithLinkContainer" })
        }
    }
});
