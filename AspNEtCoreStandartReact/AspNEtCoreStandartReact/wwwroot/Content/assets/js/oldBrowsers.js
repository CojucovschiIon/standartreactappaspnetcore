﻿var LoaderTime = 400;
function detect() {
 return parseUserAgent(navigator.userAgent);
}

function detectOS(userAgentString) {
    var rules = getOperatingSystemRules();
  var detected1=[];
for(var i=0;i<rules.length;i++)
{
    var os=rules[i];
    if(os.rule && os.rule.test(userAgentString))
    {
      return os.name;
    }
}
    return null;
}

function getNodeVersion() {
    var isNode = typeof navigator === 'undefined' && typeof process !== 'undefined';
    return isNode ? {
        name: 'node',
        version: process.version.slice(1),
        os: require('os').type().toLowerCase()
    } : null;
}

function parseUserAgent(userAgentString) {
    var browsers = getBrowserRules();
    if (!userAgentString) {
        return null;
    }
/////goood
   var detected1=[];
for(var i=0;i<browsers.length;i++)
{
    var browser=browsers[i];
    var match = browser.rule.exec(userAgentString);
    var version = match && match[1].split(/[._]/).slice(0, 3);
    if (version && version.length < 3) {
        version = version.concat(version.length == 1 ? [0, 0] : [0]);
    }
    detected1.push(match && {
                name: browser.name,
                version: version.join('.')
            });
}

     var detected;
      for (var i=0;i < detected1.length;i++)
      {
         var elem=detected1[i];
         if(elem!=null && elem.name!=null && elem.version!=null)
         {
           detected=elem;
           break;
          }
    
      }

    if (detected) {
        detected.os = detectOS(userAgentString);
    }

    return detected;
}

function getBrowserRules() {
    return buildRules([
        ['aol', /AOLShield\/([0-9\._]+)/],
        ['edge', /Edge\/([0-9\._]+)/],
        ['yandexbrowser', /YaBrowser\/([0-9\._]+)/],
        ['vivaldi', /Vivaldi\/([0-9\.]+)/],
        ['kakaotalk', /KAKAOTALK\s([0-9\.]+)/],
        ['chrome', /(?!Chrom.*OPR)Chrom(?:e|ium)\/([0-9\.]+)(:?\s|$)/],
        ['phantomjs', /PhantomJS\/([0-9\.]+)(:?\s|$)/],
        ['crios', /CriOS\/([0-9\.]+)(:?\s|$)/],
        ['firefox', /Firefox\/([0-9\.]+)(?:\s|$)/],
        ['fxios', /FxiOS\/([0-9\.]+)/],
        ['opera', /Opera\/([0-9\.]+)(?:\s|$)/],
        ['opera', /OPR\/([0-9\.]+)(:?\s|$)$/],
        ['ie', /Trident\/7\.0.*rv\:([0-9\.]+).*\).*Gecko$/],
        ['ie', /MSIE\s([0-9\.]+);.*Trident\/[4-7].0/],
        ['ie', /MSIE\s(7\.0)/],
        ['bb10', /BB10;\sTouch.*Version\/([0-9\.]+)/],
        ['android', /Android\s([0-9\.]+)/],
        ['ios', /Version\/([0-9\._]+).*Mobile.*Safari.*/],
        ['safari', /Version\/([0-9\._]+).*Safari/],
        ['facebook', /FBAV\/([0-9\.]+)/],
        ['instagram', /Instagram\ ([0-9\.]+)/]
    ]);
}

function getOperatingSystemRules() {
    return buildRules([
        ['iOS', /iP(hone|od|ad)/],
        ['Android OS', /Android/],
        ['BlackBerry OS', /BlackBerry|BB10/],
        ['Windows Mobile', /IEMobile/],
        ['Amazon OS', /Kindle/],
        ['Windows 3.11', /Win16/],
        ['Windows 95', /(Windows 95)|(Win95)|(Windows_95)/],
        ['Windows 98', /(Windows 98)|(Win98)/],
        ['Windows 2000', /(Windows NT 5.0)|(Windows 2000)/],
        ['Windows XP', /(Windows NT 5.1)|(Windows XP)/],
        ['Windows Server 2003', /(Windows NT 5.2)/],
        ['Windows Vista', /(Windows NT 6.0)/],
        ['Windows 7', /(Windows NT 6.1)/],
        ['Windows 8', /(Windows NT 6.2)/],
        ['Windows 8.1', /(Windows NT 6.3)/],
        ['Windows 10', /(Windows NT 10.0)/],
        ['Windows ME', /Windows ME/],
        ['Open BSD', /OpenBSD/],
        ['Sun OS', /SunOS/],
        ['Linux', /(Linux)|(X11)/],
        ['Mac OS', /(Mac_PowerPC)|(Macintosh)/],
        ['QNX', /QNX/],
        ['BeOS', /BeOS/],
        ['OS/2', /OS\/2/],
        ['Search Bot', /(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves\/Teoma)|(ia_archiver)/]
    ]);
}

function buildRules(ruleTuples) {
    var response=[];
    for(var i=0;i<ruleTuples.length;i++)
    {
      var tuple=ruleTuples[i];
      response.push({
            name: tuple[0],
            rule: tuple[1]
        });
    }
    return response;
}

function get_browser() {
    var Brovser_NV1 = detect();
    if (Brovser_NV1.os == 'iOS' && Brovser_NV1.os == 'Android OS')
    {
        LoaderTime = 2000;
    }
    var Brovser_NV = {name:Brovser_NV1.name,version:Brovser_NV1.version.split(".")[0]};
    var oldBrowserUrl = window.location.protocol + "//" + window.location.host + "/Main/OldBrowsers";
    if (window.location.href != oldBrowserUrl) {
        //debugger;
        switch (Brovser_NV.name) {
            case "chrome":   ///opera mini return version 27 and crome 22
                if (Brovser_NV.version < 22) {
                    window.location.href = oldBrowserUrl;
                }
                break;
            case 'ie':
                if (Brovser_NV.version < 10) {
                    window.location.href = oldBrowserUrl;
                }
                break;
            case 'opera':
                if (Brovser_NV.version < 18 || Brovser_NV.version == null) {
                    window.location.href = oldBrowserUrl;
                }
                break;
            case "firefox":
                if (Brovser_NV.version < 18) {
                    window.location.href = oldBrowserUrl;
                }
                break;
            case "safari":
                if (Brovser_NV.version < 9) {
                    window.location.href = oldBrowserUrl;
                }
                break;
            default:
                break;
        }
    }
}
get_browser();