﻿$("body").on("click", "*[data-account]", function (e) {
    e.preventDefault;
    "use strict";
   // debugger;
    var el = $(this),
        el_ajax_url = el.attr("data-account"),
        el_modal_tmpl = el.attr("data-account-tmpl");
    open_popup_box_account(el_ajax_url, el_modal_tmpl, el);
});

function open_popup_box_account(url, tmpl, sender) {
    "use strict";
    if (!tmpl) {
        tmpl = "default";
    }
    var tmpl_contents = tmpl;

    if (url == "#") {
        if (tmpl.indexOf('account_') == 0) {
            var $dialog = $('#' + tmpl_contents);
            tmpl = "short";
            $.each(sender.get(0).attributes, function (i, attrib) {
                console.log('attrib.name=' + attrib.name + '; value=' + attrib.value);

                if (attrib.name == 'data-invoice-id' || attrib.name == 'data-invoice-has-email' || attrib.name == 'data-activate_invoice_service' ||
                    attrib.name == 'data-deactivate_poshta' || attrib.name == 'data-deactivate_invoice_service') {
                    $('#btn-account-deactivate').attr(attrib.name, attrib.value);
                    //  $(').attr(attrib.name, attrib.value);
                }
                else if (attrib.name == 'data-dialog-ico') {
                    $dialog.find('.' + attrib.name).attr('class', attrib.value);
                }
                else {
                    $dialog.find('.' + attrib.name).html(attrib.value);
                }
            });
         
            if (sender.attr("data-confirmation_text")) {
                var textcontent = sender.attr("data-confirmation_text");
                $dialog[0].children[6].children[0].children[1].innerHTML = textcontent;
            }
            if (sender.attr("data-action")) 
            {
                var actiontype = sender.attr("data-action");
                if (actiontype == 'activate') {
                    $dialog[0].children[0].innerHTML = document.getElementById("serviceTextForPopup").value;
                } else/////in case if isaction deactivate
                {
                    $dialog[0].children[0].innerHTML = document.getElementById("serviceTextDeactivationForPopUp").value;
                }
                
            }
            var data = $dialog.html();
            create_popup(data, tmpl);
        }
    } else {
        $.ajax({
            url: url,
            data: { "tmpl": tmpl },
        })
            .done(function (data) {
                create_popup(data, tmpl);
            });
    }
}

$('body').on('click', "#btn-account-deactivate", function (e) {
    var activateInvoiceService = $(this).attr('data-activate_invoice_service');
    var postaDeactivation = $(this).attr('data-deactivate_poshta');
    var deactivateInvoiceService = $(this).attr('data-deactivate_invoice_service');
 
    if (activateInvoiceService == 'True')
    {
        return emailActivation(); 
    }
    if (deactivateInvoiceService=='True')
    {
       return emailDeactivation();
    }
    if (postaDeactivation=='True')
    {
        return emailPoshtaDeactivation();
    }
});


 



function emailPoshtaActivation() {
    var accountId = { AccountNumber: document.getElementById('hiddenAccountId').value };
    var poshtaMD = { generic: document.getElementById('hiddenPoshtaName').value };
    var data = {
        AccountId: document.getElementById('hiddenAccountId').value,
        GenericType: document.getElementById('hiddenPoshtaName').value
    };
    var text = $('#emailActivation').text();

    //{ AccountNumber: model, generic: poshtaMD }
    OMDAPI.util.jsonPost('/Account/PostEmailOrPoshta', data, function (data) {
        $('#emailActivation').text(text);
        $('#emailActivation').removeClass('has_loader');
        showResultDialog(data);
    }, function () { },
        "emailActivation");
};

function emailPoshtaDeactivation() {
    var data = {
        GenericType: document.getElementById('hiddenPoshtaName').value
    };
    var text = $('#emailActivation').text();
    //debugger;
    OMDAPI.util.jsonPost('/Account/DeactivatePoshta',data, function (data) {
        $('#emailActivation').text(text);
        $('#emailActivation').removeClass('has_loader');
        close_popup_box(true);
        showResultDialog(data);
    }, function () { },
        "btn-account-deactivate");
}

function userEmailActivation() {
    var data = { ActivateViaEmailOrPoshta: document.getElementById('emailValueParam').value };
    var text = $('#newEmailActivation');
    // debugger;
    OMDAPI.util.jsonPost('/Account/ActivateEmailUser', data, function (data) {
        close_popup_box(true);
        showResultDialog(data);
    });
};

function emailActivation() {
    var objectData = { ActivateViaEmailOrPoshta: document.getElementById('emailValueParam').value };
    var text = $('#usersEmail');

    OMDAPI.util.jsonPost('/Account/ActivateInvoiceEmail', objectData, function (data) {
        close_popup_box(true);
        showResultDialog(data);
    }, function () { },
        "btn-account-deactivate");
};

function emailDeactivation() {
    var objectData = { ActivateViaEmailOrPoshta: document.getElementById('emailValueParam').value };
    OMDAPI.util.jsonPost('/Account/DeactivateInvoiceEmail', objectData, function (data) {
        close_popup_box(true);
        showResultDialog(data);
    }, function () { },
        "btn-account-deactivate");
};