﻿var barValue = JSON.parse(document.getElementById("minutesCircleContentValue").value);
var bar2Value = JSON.parse(document.getElementById("gbCircleContentValue").value);
var bar1Value = JSON.parse(document.getElementById("smsCircleContentValue").value);

var color1 = '#4BB4E6';
if (parseFloat(barValue.consumed.split(',').join('.')) < 0.7) { color1 = '#ffcc00'; }
var color2 = '#4BB4E6';
if (parseFloat(bar1Value.consumed.split(',').join('.')) < 0.7) { color2 = '#ffcc00'; }
var color3 = '#4BB4E6';
if (parseFloat(bar2Value.consumed.split(',').join('.')) < 0.7) { color3 = '#ffcc00'; }

var bar = new ProgressBar.Circle(minutesCircleContent, {
    color: '#FFEA82',
    trailColor: '#eee',
    trailWidth: 14,
    duration: 1400,
    easing: 'bounce',
    strokeWidth: 14,
    text: {
        value: "<h6>" + barValue.valueMin + "</h6><h6>" +  barValue.minuteText + "</h6>"
    },
    from: { color: '#ff0008', a: 0 },
    to: { color: color1, a: 1 },
    // Set default step function for all animate calls
    step: function (state, circle) {
        circle.path.setAttribute('stroke', state.color);
    }
});

var bar1 = new ProgressBar.Circle(smsCircleContent, {
    color: '#FFEA82',
    trailColor: '#eee',
    trailWidth: 14,
    duration: 2000,
    easing: 'bounce',
    strokeWidth: 14,
    text: {
        value: "<h6>" + bar1Value.valueSmsDisp + "</h6><h6>" + bar1Value.smsText + "</h6>"
    },
    from: { color: '#ff0008', a: 0 },
    to: { color: color2, a: 1 },
    // Set default step function for all animate calls
    step: function (state, circle) {
        circle.path.setAttribute('stroke', state.color);
    }
});

var bar2 = new ProgressBar.Circle(gbCircleContent, {
    color: '#FFEA82',
    trailColor: '#eee',
    trailWidth: 14,
    duration: 3000,
    easing: 'bounce',
    strokeWidth: 14,
    text: {
        value: "<h6>" + bar2Value.valueGB + `<br>` + bar2Value.gbText + "</h6>"
    },
    from: { color: '#ff0008', a: 0 },
    to: { color: color3, a: 1 },
    // Set default step function for all animate calls
    step: function (state, circle) {
        circle.path.setAttribute('stroke', state.color);
    }
});

document.getElementById("minutesCircleContent").firstChild.style.width = "90%";
document.getElementById("smsCircleContent").firstChild.style.width = "90%";
document.getElementById("gbCircleContent").firstChild.style.width = "90%";
document.getElementById('minutesCircleContent').style.width = '120px';
document.getElementById('smsCircleContent').style.width = '120px';
document.getElementById('gbCircleContent').style.width = '120px';

bar.animate(parseFloat(barValue.consumed.split(',').join('.')));  // Number from 0.0 to 1.0
bar1.animate(parseFloat(bar1Value.consumed.split(',').join('.')));  // Number from 0.0 to 1.0
bar2.animate(parseFloat(bar2Value.consumed.split(',').join('.')));  // Number from 0.0 to 1.0
