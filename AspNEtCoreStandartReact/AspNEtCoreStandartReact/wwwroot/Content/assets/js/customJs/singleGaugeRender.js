﻿var bar2Value = JSON.parse(document.getElementById("gbCircleContentValue").value);
var color2 = '#4BB4E6';
if (parseFloat(bar2Value.consumed.split(',').join('.')) < 0.7){color2 = '#ffcc00';}
var bar2 = new ProgressBar.Circle(gbCircleContent, {
    color: '#FFEA82',
    trailColor: '#eee',
    trailWidth: 14,
    duration: 3000,
    easing: 'bounce',
    strokeWidth: 14,
    text: {
        value: "<h6>" + bar2Value.valueGB + `<br>` + bar2Value.gbText + "</h6>"
    },
    from: { color: '#ff0008', a: 0 },
    to: { color: color2, a: 1 },
    // Set default step function for all animate calls
    step: function (state, circle) {
        circle.path.setAttribute('stroke', state.color);
    }
});

document.getElementById('gbCircleContent').style.width = '100px';

bar2.animate(parseFloat(bar2Value.consumed.split(',').join('.')));  // Number from 0.0 to 1.0
