﻿var embdedHeader = setInterval(function () {
    if ($(".embedHeader").length) {
        setTimeout(showMenuHeader(), LoaderTime + LoaderTime/2);
        clearInterval(embdedHeader);
    }
}, 400);

var betaIconTimer = setInterval(function ()
{
    if ($(".beta-icon").length) {
        setTimeout(showBetaElement(), LoaderTime);
        clearInterval(betaIconTimer);
    }
}, 100);

var bodyShowTimer = setInterval(function ()
{
    if ($("#feeback-box") && $(".last-animation"))
    {
        setTimeout(showBodyApperrance(), LoaderTime);
        //resizeFunction();
        clearInterval(bodyShowTimer);
    }
},100);

function showMenuHeader()
{
    if ($(".embedHeader").length) {
        $(".embedHeader").show();
        $(".embedHeader").addClass("menuEffect");
    }
}
function showBetaElement()
{
    if ($(".beta-icon").length)
    {
    $(".beta-icon").show();
    $(".beta-icon").addClass("menuEffect");
    }
}
function showBodyApperrance() {

    if ($("#feeback-box") && $(".last-animation"))
    {
        $(".loader_center").remove();
        ///$(".loader_container").remove();
        $(".last-animation").show();
        $(".last-animation").addClass("contentEffect");

        $("#feeback-box").show();
        $("#feeback-box").addClass("contentEffect");
    }
}