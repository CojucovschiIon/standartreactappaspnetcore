﻿var isFalidNumber = false;
var isValidMin = false;
var regexNumber = new RegExp('^0{0,1}[6-7]{1}[0-9]{7}$');
$("#numberPhone").change(function () {
    if ($("#numberPhone").val() == "") {
        $("#numberPhone").css("border", "2px solid red");
        $("#insertOrangeNumber").show();
        $("#insertValidOrangeNumber").hide();
        $("#invalidPrepayOrangeNumber").hide();
        isFalidNumber = false;
        $("#errorPhoneNumberContainer").show();
    } else {
        if ($("#numberPhone").val().length > 7 && ($("#numberPhone").val().startsWith('07') ||
            $("#numberPhone").val().startsWith('06') ||
            $("#numberPhone").val().startsWith('6') ||
            $("#numberPhone").val().startsWith('7'))) {
            $("#errorPhoneNumberContainer").hide();
            $("#numberPhone").css("border", "2px solid #ccc");
            isFalidNumber = true;
        } else {
            $("#numberPhone").css("border", "2px solid red");
            $("#invalidPrepayOrangeNumber").hide();
            $("#insertOrangeNumber").hide();
            $("#insertValidOrangeNumber").show();
            $("#errorPhoneNumberContainer").show();
            isFalidNumber = false;
        }
    }
});
function loyalty_contiunue_step1() {
    // get effect type from
    var selectedEffect = 'drop';

    // Most effect types need no options passed by default
    var options = {};
    // Run the effect
    var validateNUmber = $("#numberPhone").val().match(/^0{0,1}[6-7]{1}[0-9]{7}$/);////$("#numberPhone").val().search(regexNumber) > 0;
    //debugger;
    if (validateNUmber)
    {
        isFalidNumber = true;
    }

    if (isFalidNumber) {
        var obj = { ToMsisdn: $("#numberPhone").val() };
        var nameButtonContinuestep1 = document.getElementById("loyalty_contiunue_step1").innerHTML;
        OMDAPI.util.jsonPost('/Loyalty/CheckIfPrepay', obj, function (data) {
            console.log(data);
            if (data.isSuccess == true) {
                $('#loyalty_contiunue_step1').removeClass('has_loader');
                document.getElementById("loyalty_contiunue_step1").innerHTML = nameButtonContinuestep1;
                $("#loyalty_content_step1").effect(selectedEffect, options, 500, callback1);
            } else {
                $('#loyalty_contiunue_step1').removeClass('has_loader');
                document.getElementById("loyalty_contiunue_step1").innerHTML = nameButtonContinuestep1;
                $("#insertOrangeNumber").hide();
                $("#insertValidOrangeNumber").hide();
                $("#invalidPrepayOrangeNumber").show();
                $("#errorPhoneNumberContainer").show();
            }

        }, function () { },
            "loyalty_contiunue_step1");
    }
    else {
        $("#errorPhoneNumberContainer").show();
        if ($("#numberPhone").val() == "") $("#insertOrangeNumber").show();

    }
};
function callback1() {
    setTimeout(function () {
        $("#step_bar_box1").removeClass("current");
        $("#step_bar_box2").addClass("current");
        $("#step_bar_box1").addClass("done");
        $("#step_bar_box2").removeClass("next");
        $("#loyalty_content_step2").removeAttr("style").fadeIn();
    }, 0);
};

$("#amount").change(function () {
    if ($("#amount").val() == "" || $("#amount").val() < 5) {
        $("#amount").css("border", "2px solid red");
        $("#insertMpreThan0").show();
        $("#insertLessThan100").hide();
        isValidMin = false;
        $("#errorMinutesContainer").show();
    } else {
        if ($("#amount").val() < 101) {
            $("#errorMinutesContainer").hide();
            $("#amount").css("border", "2px solid #ccc");
            $("#loyalty_contiunue_step2").removeAttr("disabled");
            isValidMin = true;
        } else {
            $("#amount").css("border", "2px solid red");
            $("#insertMpreThan0").hide();
            $("#insertLessThan100").show();
            $("#errorMinutesContainer").show();
            isValidMin = false;
        }
    }
});
function loyalty_contiunue_step2() {
    var selectedEffect = 'drop';

    if ($("#amount").val() == "" || $("#amount").val() < 5)
    {isValidMin = false;}
    else{isValidMin = true;}

    $("#msisdn_post0").val = document.getElementById("numberPhone");
    document.getElementById("msisdn_post0").value = $("#numberPhone").val();
    document.getElementById("msisdn_post1").innerHTML = $("#numberPhone").val();
    document.getElementById("minutes_post0").value = $("#amount").val();
    document.getElementById("minutes_post1").innerHTML = $("#amount").val();
    var options = {};
    // Run the effect
    if (isValidMin) {
        $("#loyalty_content_step2").effect(selectedEffect, options, 500, callback2);
    }
    else {
        $("#errorMinutesContainer").show();
        if ($("#amount").val() == "") $("#insertMpreThan0").show();
    }
};
function callback2() {
    setTimeout(function () {
        $("#step_bar_box2").removeClass("current");
        $("#step_bar_box2").addClass("done");
        $("#step_bar_box3").addClass("current");
        $("#step_bar_box3").removeClass("next");
        $("#loyalty_content_step3").removeAttr("style").hide().fadeIn();
    }, 0);
};
$("#cancel_loyalty_setp2").click(function () {
    var selectedEffect = 'slide';
    var options = {};
    $("#loyalty_content_step2").hide();
    $("#loyalty_content_step1").effect(selectedEffect, options, 500, function () {
        $("#step_bar_box1").removeClass("done");
        $("#loyalty_contiunue_step1").removeAttr("style");
        $("#step_bar_box1").addClass("current");
        $("#step_bar_box2").removeClass("current");
        $("#step_bar_box2").addClass("next");
    });

    //$("#loyalty_content_step2").removeAttr("style").hide().fadeIn();

});

$("#cancel_loyalty_setp3").click(function () {
    var selectedEffect = 'slide';
    var options = {};
    $("#loyalty_content_step3").hide();
    $("#loyalty_content_step2").effect(selectedEffect, options, 500, function () {
        $("#step_bar_box2").removeClass("done");
        $("#step_bar_box2").addClass("current");
        $("#step_bar_box3").removeClass("current");
        $("#step_bar_box3").addClass("next");
    });

    //$("#loyalty_content_step2").removeAttr("style").hide().fadeIn();

});

function isNumberKey(evt) {
    
    if (evt.currentTarget.id == "numberPhone") {
        $("#loyalty_contiunue_step1").removeAttr("disabled");
    }
    if (evt.currentTarget.id == "amount") {
        $("#loyalty_contiunue_step2").removeAttr("disabled");
    }
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}