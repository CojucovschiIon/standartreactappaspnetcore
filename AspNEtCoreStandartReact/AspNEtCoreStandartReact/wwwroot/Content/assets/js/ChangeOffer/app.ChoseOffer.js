﻿$("body").on("click", "*[data-chose-offer]", function (e) {
    e.preventDefault;
    "use strict";
    var el = $(this),
        el_ajax_url = el.attr("data-chose-offer"),
        el_modal_tmpl = el.attr("data-popup-tmpl");
    open_popup_box_chose_offer(el_ajax_url, el_modal_tmpl, el);
});
function open_popup_box_chose_offer(url, tmpl, sender) {
    "use strict";
    if (!tmpl) {
        tmpl = "default";
    }
    var tmpl_contents = tmpl;

    if (url == "#") {
        if (tmpl.indexOf("chose_offer_") == 0) {
            var $dialog = $('#' + tmpl_contents);
            tmpl = "short";

            $.each(sender.get(0).attributes, function (i, attrib) {
                console.log('attrib.name=' + attrib.name + '; value=' + attrib.value);

                if (attrib.name == 'data-change-offer-objectpost') {
                    $('#accept_and_change_plan').attr(attrib.name, attrib.value);
                    //  $(').attr(attrib.name, attrib.value);
                }
                else if (attrib.name == 'data-dialog-ico') {
                    $dialog.find('.' + attrib.name).attr('class', attrib.value);
                }
                else {
                    $dialog.find('.' + attrib.name).html(attrib.value);
                }
            });

            var data = $dialog.html();
            create_popup(data, tmpl);
        }
    } else {
        $.ajax({
            url: url,
            data: { "tmpl": tmpl },
        })
            .done(function (data) {
                create_popup(data, tmpl);
            });
    }
}

function showResultDialogChoseOffer(data) {
    if (data.isSuccess) {
        var el = $('<h1></h1>');
        el.attr('data-dialog-message', data.email);
        open_popup_box_invoice("#", "invoice_message_dialog_InvoiceWasSended", el);
    } else {
        showResultDialog(data);
    }
}
$('body').on('click', '#accept_and_change_plan', function (e) {

    var ChosePlanModel = JSON.parse($(this).attr('data-change-offer-objectpost'));

    OMDAPI.util.jsonPost('/plan/changeoffer', ChosePlanModel, function (data) {
        console.info(data);
        close_popup_box(true);

        //patch  request to finish activation plan
        if (data.patchRequest == true) {
            ConfirmOptionActivation(data);
        } else {
            showResultDialog(data);
        }
    }, function () { },
        "accept_and_change_plan");
});

function ConfirmOptionActivation(data) {
    console.info(data);
    var el = $('<h1 data-object-patch=' + JSON.stringify(data.postObject) + '  data_another=""></h1>');
    open_popup_box_patch("#", "local_patch_dialog_changePlanConfirmation", JSON.stringify(data.postObject));
}

function open_popup_box_patch(url, tmpl, sender) {

    "use strict";
    if (!tmpl) {
        tmpl = "default";
    }
    var tmpl_contents = tmpl;

    if (url == "#") {
        if (tmpl.indexOf("local_") == 0) {
            var $dialog = $('#' + tmpl_contents);

            tmpl = "short";
            $('#btn-dialog-change-offer').attr('data-object-patch', sender);
 
            var data = $dialog.html();
            //debugger;
            create_popup(data, tmpl);
        }
    } else {

        $.ajax({
            url: url,
            data: { "tmpl": tmpl }

        })
            .done(function (data) {

                create_popup(data, tmpl);

            });
    }

    $('body').on('click', '#btn-dialog-change-offer', function (e) {
        var serviceId = $(this).attr('data-object-patch');
        console.log(serviceId);

        var obj = { 'model': JSON.parse($(this).attr('data-object-patch')) };

        OMDAPI.util.jsonPost('/Plan/PatchChangeSubscription', obj, function (data) {
            close_popup_box(true);
            showResultDialog(data);
        }, function () { },
            "btn-btn-dialog-change-offer-grouped");

    });
}

