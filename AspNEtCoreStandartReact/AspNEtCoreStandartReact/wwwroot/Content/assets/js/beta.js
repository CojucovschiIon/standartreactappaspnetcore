$(document).ready(function(){

	var langb = $('html').attr('lang');

    var l_ro = {
    p_init:{},
    lang_key:"ro",
        b_icon:'<img src="/Content/assets/img/beta-ico-ro.png" class="fi-dtop"><img src="/Content/assets/img/beta-ico-ro.png" class="fi-mobile">',
        b_banner:'<img src="/Content/assets/img/banner-ro.gif" style="width:100%;">',
    beta_title: "myOrange Beta",
        beta_ico: "/Content/assets/img/beta-ico-ro.png",
        banner_src:"/Content/assets/img/banner-ro.gif",
		beta_text_msg:"Testează My Orange pe web. Spune-ne ce crezi, folosind formularul de feedback.",
		b_ok_cta:"Accesează versiunea anterioară",
		cancel_cta:"OK",
  }

	var l_ru={
    p_init:{},
    lang_key:"ru",
        b_icon:'<img src="/Content/assets/img/beta-ico-ru.png" class="fi-dtop"><img src="/Content/assets/img/beta-ico-ru.png" class="fi-mobile">',
        b_banner:'<img src="/Content/assets/img/banner-ru.gif" style="width:100%;">',
    beta_title: "myOrange Beta",    
        beta_ico: "/Content/assets/img/beta-ico-ru.png",
        banner_src:"/Content/assets/img/banner-ru.gif",
		beta_text_msg:"Протестируйте новую версию My Orange. Расскажите нам, что вы думаете и оставьте свой отзыв.",
		b_ok_cta:"Перейти к предыдущей версии",
    cancel_cta:"OK",
  }

	var l_en={    
    p_init:{},
    lang_key:"en", 
        b_icon:'<img src="/Content/assets/img/beta-ico-en.png" class="fi-dtop"><img src="/Content/assets/img/beta-ico-en.png" class="fi-mobile">',
        b_banner:'<img src="/Content/assets/img/banner-en.gif" style="width:100%;">',
    beta_title: "myOrange Beta",
    lang_title: "",
        beta_ico: "/Content/assets/img/beta-ico-en.png",
        banner_src:"/Content/assets/img/banner-en.gif",
		beta_text_msg:"Discover the new version of My Orange. Tell us what you think by using the feedback form.",
		b_ok_cta:"Previous version",
		cancel_cta:"OK",    

  }
  var lngt = l_ro;
	function apply_lang(langb){
		
		switch(langb) {
      case 'ro':   		var lngt = l_ro;  break;
      case 'ro-RO':   var lngt = l_ro;  break;
      case 'ru':   		var lngt = l_ru;  break;
      case 'ru-RU':   var lngt = l_ru;  break;
      case 'en':   		var lngt = l_en;  break;
      case 'en-US':   var lngt = l_en;  break;
      //default: 		 		var lngt = l_ro;  break;
  	}
		
		$("body").prepend(templ);
  	
  	$('*[data-lang]').each(function(){
        var el=$(this), el_p = el.attr("data-lang")
        el.html(lngt[el_p]);
    });

	}
	setTimeout(function(){
		apply_lang(langb);
	},100)

    var stylesheet ='/Content/assets/css/beta.css';
	var templ=		'<div id="beta-box" class="theme-b">'+
								
								'<div class="beta-icon">'+
									'<div class="inner ">'+
									'<span data-lang="b_icon"></span>'+
									'</div>'+
								'</div>'+

								'<div class="beta-contents">'+
									
									'<div class="inner">'+
										
										'<!--step 1: form-------------------------------------->'+
										'<div class="flow-step active">'+
											'<span data-lang="b_banner"></span>'+
											//'<img src="https://cdn.orange.md/myorange/beta/widget/assets/img/banner-'+lngt.lang_key+'.gif" style="width:100%;">'+
											'<div style="padding:20px;">'+
												'<br />'+
												'<span data-lang="beta_text_msg"></span>'+
												'<br />'+
												'<br />'+
												'<div class="message-send noselect"  data-lang="cancel_cta" data-scope="close-flow"> </div>'+
												'<br />'+
        '<center><b  style="cursor:pointer;" onClick=\'document.location.href="/login/RedirectToMyOrangeCID"\' data-lang="b_ok_cta"></b></center>' + //https://my.orange.md/ro/my-account
												'<br />'+
												'</div>'+
										'</div>'+

									'</div>'+
								'</div>'+
						  '</div>';



	$("head").append('<link rel="stylesheet" type="text/css" href="'+stylesheet+'?v=3143">');
	
		





	function reset_flow(){
		$('#beta-box .beta-contents').removeClass('active');
  	$('#beta-box .flow-step').removeAttr('style');
  	$('#beta-box .flow-step.active').removeClass('active');
  	$('#beta-box .flow-step:first').addClass('active');
	}
	
	//Events:
 



	/////////////////////////////////////////////////////////////////////////
	// $('body').on('click', '.message-send', function(){
	// 	var text = $('#beta-g-text').val();
	// 	var votes = $('#stars-rate > span.active').length;
	// 	var page_title = $('title').text();
	// 	var site_url = window.location.href;
	// 	site_url = site_url.replace("https://", "");
	// 	site_url = site_url.replace("http://", "");
	// 	console.log(page_title);

	// 	send_greeting(text, votes, page_title, site_url);
		
	// });
	/////////////////////////////////////////////////////////////////////////
	$("body").on('click', '#beta-box .beta-icon', function(){
        $('#beta-box .beta-contents').addClass('active');
        $('.beta-contents').removeAttr('style');
	});
	/////////////////////////////////////////////////////////////////////////
	var mouse_is_inside = false;
  
  $('#beta-box .beta-contents > .inner').hover(function(){ 
      mouse_is_inside=true; 
  }, function(){ 
      mouse_is_inside=false; 
  });

  $("body").mouseup(function(){ 
    if(! mouse_is_inside) {
    	reset_flow();

    };
  });
  /////////////////////////////////////////////////////////////////////////
  $("body").on('click', '*[data-scope="close-flow"]', function(){
		reset_flow();
  });

  /////////////////////////////////////////////////////////////////////////
	$('body').on('keyup', function(e){
    
    if(e.which == 27){
        // Close my modal window
        reset_flow();
    }

    if (e.keyCode == 13) {
        // Do something
        if($('.message-send').is(':visible')){
					$('.message-send').trigger('click');
        }else{
     			reset_flow();   	
        }
    }
});
});