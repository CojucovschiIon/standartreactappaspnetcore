﻿$("body").on("click", "*[data-favorite]", function (e) {
    e.preventDefault;
    "use strict";
    var el = $(this),
        el_ajax_url = el.attr("data-favorite"),
        el_modal_tmpl = el.attr("data-popup-tmpl");
    open_popup_box_favorite(el_ajax_url, el_modal_tmpl, el);
});
function open_popup_box_favorite(url, tmpl, sender) {
    "use strict";
    if (!tmpl) {
        tmpl = "default";
    }
    var tmpl_contents = tmpl;

    if (url == "#") {
        if (tmpl.indexOf("favorite_") == 0) {
            var $dialog = $('#' + tmpl_contents);

            tmpl = "short";

            $.each(sender.get(0).attributes, function (i, attrib) {
                console.log('attrib.name=' + attrib.name + '; value=' + attrib.value);

                if (attrib.name == 'data-option-form-id' || attrib.name == 'data-option-id' || attrib.name == 'data-is-number-already-active' || attrib.name == 'data-option-redirect-link' || attrib.name == 'data-option-activation-type') {
                    $('#btn-dialog-modify-favorite, #btn-dialog-add-favorite-number, #btn-dialog-add-wind').attr(attrib.name, attrib.value);
                    //  $(').attr(attrib.name, attrib.value);
                }
                else if (attrib.name == 'data-dialog-ico') {
                    $dialog.find('.' + attrib.name).attr('class', attrib.value);
                }
                else {
                    $dialog.find('.' + attrib.name).html(attrib.value);
                }
            });

            if (sender.attr("data-is-number-already-active") == "True") {
                $dialog[0].children[6].children[0].children[1].innerHTML = document.getElementById("modify_favorite_number_text").value;
            }
            else {
                $dialog[0].children[6].children[0].children[1].innerHTML = document.getElementById("add_newFavoriteNUmber_Text").value;
            }

            var data = $dialog.html();
            //debugger;
            create_popup(data, tmpl);
        }
    } else {
        $.ajax({
            url: url,
            data: { "tmpl": tmpl },
        })
            .done(function (data) {
                create_popup(data, tmpl);
            });
    }
}

$('body').on('click', '#btn-dialog-add-favorite-number', function (e) {
    var redirect = $(this).attr('data-option-redirect-link');
    var serviceId = $(this).attr('data-option-id');
    var activationType = $(this).attr('data-option-activation-type');
    var isPromoActivation = $(this).attr('data-option-ispromo');

    var obj = { ServiceId: serviceId };
    if (activationType != 'isPerepay') {
        obj.ActivationType = activationType;
    }

    OMDAPI.util.jsonPost('/OptionsAndServices/ManageOption', obj, function (data) {
        close_popup_box(true);
        ///show another response popup if is promo
        if (isPromoActivation != null || isPromoActivation == true) {
            showResultDialogPromo(data);
        } else {
            showResultDialog(data);
        }
        redirectWhenMagicNumber(data, serviceId, redirect);
    }, function () { },
        "btn-dialog-add-option");
});


$('body').on('click', '#btn-dialog-modify-favorite', function (e) {
    ///var tempButtonValue = e;
    //debugger;
    var model = $('#' + $(this).attr('data-option-form-id')).serialize();//{ Id: $(this).attr('data - option - id') };
    //  console.log(model);

    OMDAPI.util.jsonPost('/FavoriteNumbers/FavoriteNumbers', model, function (data) {
        console.info(data);
        close_popup_box(true);
        showResultDialog(data);
    }, function () { },
        "btn-dialog-modify-favorite");
});


$('body').on('click', '#btn-dialog-add-wind', function (e) {
    ///var tempButtonValue = e;
    //debugger;
    var model = $('#' + $(this).attr('data-option-form-id')).serialize();//{ Id: $(this).attr('data - option - id') };
    //  console.log(model);

    OMDAPI.util.jsonPost('/FavoriteNumbers/WindNumbers', model, function (data) {
        console.info(data);
        close_popup_box(true);
        showResultDialog(data);
    }, function () { },
        "btn-dialog-add-wind");
});






