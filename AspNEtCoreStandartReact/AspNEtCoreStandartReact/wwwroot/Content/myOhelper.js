$(function(){
    $('body')
    .addClass('omd-style')
    .addClass('omd-style-padding')
    .prepend('<div id="mosse-header"></div>')
    .append(
        '<div id="mosse-footer"></div>' +
        '<script async type="text/javascript" src="https://www.orange.md/emea/load.js" async="false"></script>'
    );
    var lang = document.documentElement.lang;
    var pathname = window.location.pathname + window.location.search;

    ///var current_language = "/main/changelanguage" + "?culture=" + lang + "&returnUrl=" + pathname;
    var switch_lang_href_ro = "/main/changelanguage?culture=ro";///&returnUrl=" + pathname;
    var switch_lang_href_ru = "/main/changelanguage?culture=ru";///&returnUrl=" + pathname;
    var switch_lang_href_en = "/main/changelanguage?culture=en";///&returnUrl=" + pathname;
    
    $('body').on('click', '.lng_switch_item_ro', function () {
        document.cookie="GUEST_LANGUAGE_ID = ro_RO";
    });
    
    $('body').on('click', '.lng_switch_item_ru', function () {
        document.cookie="GUEST_LANGUAGE_ID = ru_RU";
    });

    $('body').on('click', '.lng_switch_item_en', function () {
        document.cookie="GUEST_LANGUAGE_ID = en_EN";
    });


    var showHeader = true;
    var showFooter = true;
    setTimeout(function () {
        $('#mosse-header').addClass('active');

        $('#mosse-header a').each(function () {
            $(this).attr('rel', 'noopener');
        });

        $('#mosse-header img').each(function () {
            if (!$(this).attr('alt')) {
                $(this).attr('alt', 'Orange')
            }
        });
    }, 1000)
})

$('body').on('click', '*[data-role="expand-txt"]', function () {
    var el = $(this);
    //el.parent().children('.expand-txt').removeClass('hidden');
    el.parent().children('.expand-txt').fadeIn(300);
    el.remove();
})
//txt expander
$('body').on('click', '*[data-role="expand-box"]', function () {
    var el = $(this);
    el.parent().children('.expand-box').toggleClass('hidden'); // needded for external button to expand/collapse 
    el.closest('.expand-box').toggleClass('hidden');  // needded for inner  button to collapse

})



$('body').on('click', '.accordion-item > .accordion-header', function () {
    var el = $(this);
    el_parent = $(this).parent('.accordion-item');
    if (el_parent.hasClass('active')) {
        el_parent.removeClass('active')
        el_parent.children('.accordion-contents').slideUp(300);

    } else {


        el_parent.children('.accordion-contents').slideDown(300);
        el_parent.addClass('active')

    }
});
//open pedefinex expanded accordions
$(document).ready(function () {
    $('.accordion-item.active').each(function () {
        $(this).find('.accordion-contents').slideDown(300);
    });
});


    /////////////////////////////////////////////////////////////////////////////
    // accordion expander/contracter
$('body').on('click', '*[data-role="accordion-container"] *[data-role="accordion-trigger"]', function () {
    var el = $(this);

    el
        .parent('*[data-role="accordion-container"]')
        .children('*[data-role="accordion-content-container"]')
        .toggleClass('active');

    el
        .find('span.icon-arrow-down')
        .toggleClass('icon-arrow-up');
})



