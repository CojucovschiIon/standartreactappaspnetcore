import React from 'react';


export default class MagicNumberExperience extends React.Component{

    render(){
        return(
            <div className="flex-container">
                    <div className="w-70">
                        <div className="mn-num-item">
                            <div><span className="icon icon-improvement"></span></div>
                            <div>
                                <div className="inner-item">
                                    <div data-scope="title">Tarif pentru apel spre Numărul Magic</div>
                                </div>
                            </div>
                        </div>
                        <style>
                            {`
                            #price-evo-widget .container::after {
                                background: transparent !important;
                            }
                            `}
                        </style>
                            <div id="price-evo-widget">

                                <div>
                                    <div className="container">
                                        <div className="flex-container ">
                                            <div><span className="flow-bullet icon icon-Modifier-favorite txt-xxl-size"></span> </div>
                                            <div><span className="strong  ">6 luni - 1 an</span></div>
                                            <div><span className="strong">60 bani/minut</span></div>
                                        </div>
                                        <div className="flex-container active">
                                            <div><span className="flow-bullet icon icon-Modifier-favorite txt-xxl-size"></span> </div>
                                            <div><span className="strong ">1 - 2 ani</span></div>
                                            <div><span className="strong ">50 bani/minut</span></div>
                                        </div>
                                        <div className="flex-container ">
                                            <div><span className="flow-bullet icon icon-Modifier-favorite txt-xxl-size"></span> </div>
                                            <div><span className="strong">2 - 3 ani</span></div>
                                            <div><span className="strong">40 bani/minut</span></div>
                                        </div>
                                        <div className="flex-container ">
                                            <div><span className="flow-bullet icon icon-Modifier-favorite txt-xxl-size"></span> </div>
                                            <div><span className="strong">3 - 4 ani</span></div>
                                            <div><span className="strong">30 bani/minut</span></div>
                                        </div>
                                        <div className="flex-container ">
                                            <div><span className="flow-bullet icon icon-Modifier-favorite txt-xxl-size"></span> </div>
                                            <div><span className="strong">4 - 5 ani</span></div>
                                            <div><span className="strong">20 bani/minut</span></div>
                                        </div>
                                        <div className="flex-container ">
                                            <div><span className="flow-bullet icon icon-Modifier-favorite txt-xxl-size"></span> </div>
                                            <div><span className="strong">5 - 6 ani</span></div>
                                            <div><span className="strong">10 bani/minut</span></div>
                                        </div>
                                        <div className="flex-container ">
                                            <div><span className="flow-bullet icon icon-Modifier-favorite txt-xxl-size"></span> </div>
                                            <div><span className="strong">mai mult 6 ani</span></div>
                                            <div><span className="strong">Gratuit</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div className="s_60"></div>
                    </div>
                    <div className="s_60"></div>
                </div>
        );
    }
}



