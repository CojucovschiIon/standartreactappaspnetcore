import React from 'react';



export default class MagicNumberContent extends React.Component{
    render(){
        return(
            <div className="flex-container">
                <div className="w-50">
                        <div className="mn-num-item">
                                <div><span className="icon icon-lightbulb-sh"></span></div>
                                <div>
                                    <div className="inner-item">
                                        <div data-scope="title">Numărul Magic</div>

                                            <div data-scope="sub-title-cta" className="mn-val">
                                                    <div data-scope="modify-accordion-box">
                                                        <button className="clean no-padding button-modify " data-scope="open-edit-form">
                                                            <span className="icon icon-unlimited-calls"></span> 060031404 <span className="icon icon-Pencil"></span>
                                                        </button>
                                                        <div data-scope="modify-form">
                                                                <button data-scope="open-edit-form-trigger">Anulează</button>
                                                                &nbsp;
                                                                <div className="button cta with-loader" data-favorite="#" data-popup-tmpl="favorite_favoriteNumbers_modify" data-option-id="SPO_MAGIC_NUMBER" data-option-name="Numărul Magic" data-option-description="Doreşti să continui modificarea numărului?" data-option-price="50" data-is-number-already-active="True" data-option-action="Confirmare" data-option-form-id="form-favorite-number-0">
Modifică                                                        </div>
                                                            <div className="s_10"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                    </div>
                                    <div className="s_20"></div>
                                </div>
                            </div>
                        </div>
                        <div className="w-50">
                            <div className="mn-num-item">
                                <div><span className="icon icon-call"></span></div>
                                <div>
                                    <div className="inner-item">
                                        <div data-scope="title">Tariful curent </div>
                                        <div data-scope="sub-title-cta">
50 bani                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="s_20"></div>
                        </div>
            </div>
        );
    }
}


