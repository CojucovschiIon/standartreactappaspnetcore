import React from 'react';


import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';


export default class LastInvoice extends React.Component{
    render(){
        return(
        <div>
            <div className="flex-container">
                <div>
                    <div id="dashboard-widget-last-invoice">
                        <ul className="inner-contents">
                            <li>
                                <h2>Ultima factură</h2>
                            </li>
                            <li>
                                <span className="s_20"></span>
                                <h3 className="txt-color-cta">360.00 MDL</h3>
                                <h6>Numărul facturii: 75319741</h6>
                                    <h6>Data emiterii: 05.12.2019</h6>
                                <span className="s_20"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div className="flex-container">
                <div>
                    <div>
                        <span className="s_20"></span>
                        <div className="topup-history-details-item  animated fadeIn">
                            <div className="flex top">
                                <div className="w-30">
                                    <div className="padding_0_0_0_0">
                                    <HighchartsReact highcharts={Highcharts} 

                                    containerProps={{ style: { width:"100%"} }}

                                    options={{
                                            chart: {
                                                styleModel:true,
                                                plotBackgroundColor: null,
                                                plotBorderWidth: 0,
                                                plotShadow: false,
                                                height:200,
                                                //width:200,
                                            },
                                            title: {
                                                useHTML: true,
                                                text:'',
                                                align: 'center',
                                                verticalAlign: 'middle',
                                                y:0
                                            },
                                            plotOptions: {
                                                pie: {
                                                    colors: ["rgba(255, 210, 0, 1)",///yellow
                                                     "rgba(241, 110, 0, 1)"///cta
                                                    ],
                                                    dataLabels: {
                                                        enabled: true,
                                                        distance: -50,
                                                        style: {
                                                            fontWeight: 'bold',
                                                            color: 'white'
                                                        }
                                                    },
                                                    startAngle: 0,
                                                    endAngle: 0,
                                                    size: '120%'
                                                }
                                            },
                                            credits: {
                                                enabled: false,
                                            },
                                            tooltip: {
                                                enabled: false
                                            },
                                            series: [{
                                                type: 'pie',
                                                name: '',
                                                innerSize: '50%',
                                                data: [
                                                    ['', 1 ],
                                                    ['', 2 ]
                                                ]
                                            }],
                                            exporting: { enabled: false }
                                        }}/>
                                    </div>
                                </div>
                                <div className="w-50">
                                    <div className="history-item-b">
                                        <div>
                                            <span className="icon icon-record txt-color-digital_func_yellow"></span>
                                        </div>
                                        <div>
                                            <div className="flex vertical">
                                                <div><span>Abonament şi servicii</span></div>
                                            </div>
                                        </div>
                                        <div>
                                            <span>100.00 MDL</span>
                                        </div>
                                    </div>
                                    <div className="history-item-b">
                                        <div>
                                            <span className="icon icon-record txt-color-cta"></span>
                                        </div>

                                        <div>
                                            <div className="flex vertical">
                                                <div><span>Consum în afara abonamentului</span></div>
                                            </div>
                                        </div>

                                        <div>
                                            <span>200.00 MDL</span>
                                        </div>
                                    </div>

                                    <hr />
                                    <div className="history-item-b">

                                        <div>
                                        </div>

                                        <div>
                                            <div className="flex vertical">
                                                <div><strong className="txt-xl-size">Suma totală spre plată</strong></div>
                                            </div>
                                        </div>

                                        <div>
                                            <strong className="txt-xl-size txt-color-cta">360.00 MDL</strong>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>



                </div>
            </div>

            <div className="flex-container">
                <div>
                    <span className="s_20"></span>
                        <div className="button cta with-loader" id="activatebutton">Vezi factura</div>

                    &nbsp;&nbsp;&nbsp;
                            <div className="button inverted with-loader" id="activatebutton" data-invoice="#" data-invoice-user-id="6352146" data-invoice-id="75319741" data-popup-tmpl="invoice_email_confirmation">Transmite pe email</div>
                    <span className="s_10"></span>
                    <span className="icon icon-info"></span>&nbsp;&nbsp;<strong className="txt-m-size"><a target="_blank" style={{color:'#ff7900'}} href="https://www.orange.md/?p=1&amp;c=8&amp;sc=20150&amp;l=1">Cum citim factura?</a></strong>
                    <span className="s_20"></span>
                </div>
            </div>

        </div>
           );
    }
}

