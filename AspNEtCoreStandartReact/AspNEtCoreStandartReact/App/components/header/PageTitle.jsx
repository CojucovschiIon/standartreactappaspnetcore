import React from 'react';
import { connect } from 'react-redux';


class PageTitle extends React.Component{

render(){
    return(
            <section id="title-box">
                <div className="gridContainer clearfix">
                    <div className="dt">
                        <h1 className="display-1">{this.props.PageName}</h1>
                    </div>
                    <div className="m local-nav-expander">
                        <ul>
                            <li>
                                <h1 className="display-1 inline">Acasă</h1>
                            </li>
                            <li>
                                <h1><em className="fa fa-caret-down" aria-hidden="true"></em></h1>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
        )}

}



function mapStateToProps(state){
    ///console.log("Page title",state);
    return{
        PageName:state.pageNameReducer.PageName,
    }
  }

  
  export default connect(mapStateToProps,{})(PageTitle);
  
