import React from 'react';

export default class PersonalBar extends React.Component{
constructor(props)
{
    super(props);
    this.onLogOutClick=this.onLogOutClick.bind(this);
    this.onDetailsClick=this.onDetailsClick.bind(this);

}

onLogOutClick()
{
 console.log('TO Do LOGOUT');

}
onDetailsClick()
{
    console.log('Details is clicked');
}

render(){
    return(
<section id="personal-bar">
        <div className="gridContainer clearfix">
            <div className="s_10"></div>
            <div className="flex-container personal-infos">
                <div>
                    <div className="inner-item">
                            <div data-scope="l-title">Salut,</div>
                            <div id="userMsisdn" data-scope="l-title">060031432</div>
                                    <h6>Valabilitate perioadă inactivă: 22.05.2020</h6>
                    </div>
                </div>
                <div>
                    <button onClick={this.onDetailsClick} className="dt">Detalii</button>
                </div>
                <div>
                    <button id="btn-logout" className="clean dt no-padding right" onClick={this.onLogOutClick} >
                        <i className="fa fa-sign-out" aria-hidden="true"></i>
                        Deconectare 
                    </button>
                </div>
            </div>
            <div className="s_10"></div>
        </div>
    </section>

    );
}



}