import React from "react";
import {Link } from 'react-router-dom';
import { connect } from 'react-redux';

import $ from 'jquery';
import priorityNav from 'priority-nav';

class LocalNavigationBox extends React.Component{
   
    constructor(props){
        super(props);
        this.animateLocalNAvigationBox=this.animateLocalNAvigationBox.bind(this);
    }

    // componentDidCatch(){
    //     console.log("componentDidMount");
    // }
    // componentWillUpdate(){
    //     console.log("componentWillUpdate");
    // }

    componentDidMount()
    {
        //this.animateLocalNAvigationBox();
    }


    render(){
        this.animateLocalNAvigationBox(this.props.currentLanguage["Header.MoreMenu"]);
        return(
            <section id="local-nav-box">
                <div className="gridContainer">
                    <nav className="nav dt" style={{display: 'block'}}>
                        <div className="wrapper resize-drag nav-wrapper priority-nav" instance="0">
                            <ul className="nav-ul" id="top-nav-ul">
                                <li><Link to="/">{this.props.currentLanguage["Dashboard.PageName"]}</Link></li>
                                <li><Link to="/balance/currentbalance">{this.props.currentLanguage["MyResources.PageName"]}</Link></li>
                                <li><Link to="/promo/promo">{this.props.currentLanguage["Promo.PromoPageName"]}</Link></li>
                                <li><Link to="/optionsandservices/optionsservicelist">{this.props.currentLanguage["Options&Services.PageName"]}</Link></li>
                                <li><Link to="/favoritenumbers/favoritenumbers">{this.props.currentLanguage["MagicNumber.MagicNumberPageName"]}</Link></li>
                                <li><Link to="/payment/topup">{this.props.currentLanguage["PaymentTopUp.Title"]}</Link></li>
                                <li><Link to="/payment/recharge">{this.props.currentLanguage["RechargeHistory.PageNameText"]}</Link></li>
                                <li><Link to="/plan/currentplan">{this.props.currentLanguage["Plan.PageName"]}</Link></li>
                                <li><Link to="/account/accountinfo">{this.props.currentLanguage["Account.AccountInfo"]}</Link></li>
                            </ul>
                            <span className="nav__dropdown-wrapper priority-nav__wrapper" aria-haspopup="false">
                                <button aria-controls="menu"
                                type="button" 
                                className="nav__dropdown-toggle priority-nav__dropdown-toggle priority-nav-is-hidden" 
                                data-scope="menu-sub"
                                data-menu-sub-id="s"
                                prioritynav-count="0">
                                   Mai mult
                                </button>
                                <ul aria-hidden="true" className="nav__dropdown priority-nav__dropdown"></ul>
                            </span>
                        </div>
                    </nav>
                </div>
                <div className="m">
                                <div className="mobile-slide-menu">
                                    <div className="side-a">
                                        <ul className="local-nav-contents">
                                            
                                            <li><Link to="/">{this.props.currentLanguage["Dashboard.PageName"]}</Link></li>
                                            <li><Link to="/balance/currentbalance">{this.props.currentLanguage["MyResources.PageName"]}</Link></li>
                                            <li><Link to="/promo/promo">{this.props.currentLanguage["Promo.PromoPageName"]}</Link></li>
                                            <li><Link to="/optionsandservices/optionsservicelist">{this.props.currentLanguage["Options&Services.PageName"]}</Link></li>
                                            <li><Link to="/favoritenumbers/favoritenumbers">{this.props.currentLanguage["MagicNumber.MagicNumberPageName"]}</Link></li>
                                            <li><Link to="/payment/topup">{this.props.currentLanguage["PaymentTopUp.Title"]}</Link></li>
                                            <li><Link to="/payment/recharge">{this.props.currentLanguage["RechargeHistory.PageNameText"]}</Link></li>
                                            <li><Link to="/plan/currentplan">{this.props.currentLanguage["Plan.PageName"]}</Link></li>
                                            <li><Link to="/account/accountinfo">{this.props.currentLanguage["Account.AccountInfo"]}</Link></li>
                                            <li><Link to="/main/logout"><i className="fa fa-sign-out" aria-hidden="true"></i> {this.props.currentLanguage["Header.LogOut"]}</Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
            </section>
        );

    }

    animateLocalNAvigationBox(moreText)
    {
        console.log("initilize priority nav");
        
        setTimeout(function () {
            $('#local-nav-box').show();
        }, 1000)


        var menu_box = $('#local-nav-box .mobile-slide-menu');
        menu_box.slideUp('fast');

        
        $('body').on('click', '.local-nav-expander', function () {
            menu_box.slideToggle(400);
            $(this).find('i.fa-caret-down').toggleClass("fa-caret-up");
            $('body').addClass('no-scroll-x');

            if (menu_box.hasClass('active')) {

                menu_box.removeClass('active')
                $('#menu-bg-overlay').remove();
                $('.back-bt').trigger('click');
                $('body').removeClass('no-scroll-x');
            } else {
                menu_box.addClass('active')
                //$('body').prepend('<div id="menu-bg-overlay"></div>');
            }
        });

        var nav=priorityNav.init({
            mainNavWrapper: ".nav-wrapper", // mainnav wrapper selector (must be direct parent from mainNav)
            mainNav: ".nav-ul", // mainnav selector. (must be inline-block)
            navDropdownLabel: moreText,//this.props.currentLanguage["Header.MoreMenu"],//"Mai Mult",
            navDropdownClassName: "nav__dropdown", // class used for the dropdown.
            navDropdownToggleClassName: "nav__dropdown-toggle", // class used for the dropdown toggle.
        });


        $("body").on("click", '.mobile-slide-menu .sub-list > li', function (e) {

            $('.mobile-slide-menu .sub-list > li.active').removeClass('active')
              var el = $(this);
              var el_name = el.find('span').text();
              var el_parent_cat_id = el.closest('.sub-item').attr("data-menu-sub-id");
              var  el_parent_cat_name = $('*[data-menu-sub-id="' + el_parent_cat_id + '"] > a:first').text();
    
            el.addClass('active');
            //Title
            $('.m.local-nav-expander h1.display-1').text(el_name)
            $('#title-box  .dt h1').html(el_name);
    
            //URL
            //var href = '' + el_parent_cat_name + '/' + el_name;
            //history.pushState('', '', '/eshop.orange.md/' + href);
    
    
        });
    
        $('body').on('click', '*[data-scope="mobile-menu-sub-back"]', function () {
            $('.mobile-slide-menu > .side-a.inactive').removeClass('inactive');
            $('.mobile-slide-menu > .side-b.active').removeClass('active');
        
        });

        $('body').on('click', '*[data-scope="mobile-menu-sub"]', function () {

            $('.mobile-slide-menu > .side-b').html('');
            $('.mobile-slide-menu > .side-b').css('minHeight', $('.mobile-slide-menu').height());
            //$('.mobile-slide-menu > .side-b').html('');
        
             var el = $(this);
             var el_sub_id = el.attr('data-menu-sub-id');
             var   el_sub_box = $('#local-nav-sub-box-contents');
             var   el_sub_box_item = $('#local-nav-sub-box-contents > .sub-item[data-menu-sub-id="' + el_sub_id + '"] .sub-list').html();
        
            if (el_sub_id && el_sub_box && el_sub_box_item) {
                //console.log('tru');
                $('.mobile-slide-menu > .side-b').append('<div class="back-bt" data-scope="mobile-menu-sub-back"><div class="gridContainer clearfix"><span class="icon icon-arrow-previous"></span> &nbsp;&nbsp;<span class="inline inverted " >Back</span></div></div><div class="sub-list">' + el_sub_box_item + '</div>');
                $('.mobile-slide-menu > .side-a').addClass('inactive');
                $('.mobile-slide-menu > .side-b').addClass('active');
        
            }
        });
    }
}
function mapStateToProps(state){
    return {
        currentLanguage:state.languageDexReducer.CurrentLanguage
    }

}

export default connect(mapStateToProps,{})(LocalNavigationBox);

