import React from 'react';
import $ from "jquery";

export default class NotifyBox extends React.Component
{
    constructor(props){
        super(props);
        this.onCloseIsCliked=this.onCloseIsCliked.bind(this);
    }

    onCloseIsCliked(){
        $('#notify-box').slideUp(300);
    }
    render(){
        return(
            <section id="notify-box" className="warn">
                <div className="gridContainer clearfix">
                    <div className="flex-container notify-box">
                        <div>
                            <em className="icon icon-Warningimportant"></em>
                        </div>
                        <div>
                            <div className="inner-item">
                                <div data-scope="title">
                                    La moment poţi doar primi apeluri. Pentru a efectua apeluri, te rugăm să <a style={{color:'#ff7900'}} href="/payment/topup">reîncarci</a> contul. Reîncarcă de la 60 MDL şi primeşti 40% reducere la apelurile în reţea. 
                                </div>
                            </div>
                        </div>
                        <div>
                            <button onClick={this.onCloseIsCliked} className="clean no-padding" data-scope="close-self" aria-hidden="true"><em className="icon icon-delete"></em></button>
                        </div>
                    </div>
                </div>
            </section>
        );
    }


}