import React from 'react';


export default class Banner extends React.Component{
  constructor(props){
      super(props);
      this.detailsCliked=this.detailsCliked.bind(this);
  }
  
  detailsCliked(){
      console.log("Delails is clickeds");
  }
  
    render(){
        return(
            <div>
                <div id="dashboard-widget-bonus-a">
                    <div className="inner">
                        <div className="flex-container">
                            <div>
                                <h2 className="display-3">Activează acum ofertele speciale</h2>
                                <span className="s_40"></span>
                                <button onClick={this.detailsCliked}>Detalii</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}