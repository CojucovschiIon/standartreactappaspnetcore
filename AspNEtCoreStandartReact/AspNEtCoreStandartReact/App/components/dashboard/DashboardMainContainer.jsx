import React from 'react';

import MyResources from "../dashboard/components/MyResources.jsx";
import AccountBalance from "../dashboard/components/AccountBalance.jsx";
import TarrifPlan from "../dashboard/components/TarrifPlan.jsx";
import OptionsAndServices from "../dashboard/components/OptionsAndServices.jsx";
import FavoriteNumbers from "../dashboard/components/FavoriteNumbers.jsx";
import LastInvoice from "../dashboard/components/LastInvoice.jsx";

export default class DasboardMainContainer extends React.Component{

    render(){
        return(
            <div className="dashboard-flex">
                    <div className="flex between wrap">
                        <MyResources /> 
                        <AccountBalance />
                        <TarrifPlan />
                        <OptionsAndServices />
                        <FavoriteNumbers />
                        <LastInvoice/>
                    </div>
                </div>
        );
    }

} 