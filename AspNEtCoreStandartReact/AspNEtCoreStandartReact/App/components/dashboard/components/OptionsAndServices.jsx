import React from 'react';

export default class OptionsAndServices extends React.Component{
constructor(props){
    super(props);
    this.onGesionateCliked=this.onGesionateCliked.bind(this);
}

render(){
    return(
        <div>
            <div id="dashboard-widget-ons">
                <h2 className="display-3">Opțiunile şi serviciile mele</h2>
                    <ul className="normal-circles">
                            <li><h5>Roaming</h5></li>
                            <li><h5>Cine a sunat?</h5></li>
                            <li><h5>Notificare Prezenţă</h5></li>
                    </ul>
                    <button onClick={this.onGesionateCliked}>Gestionează opțiunile şi serviciile</button>
            </div>
        </div>
    );
}
onGesionateCliked(){
    console.log('gestionate is cliked');
}
}

