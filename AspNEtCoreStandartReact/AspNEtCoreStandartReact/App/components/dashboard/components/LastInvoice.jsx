import React from 'react';

export default class LastInvoice extends React.Component{
    constructor(props){
        super(props);
        this.onLookHistoryIsCLiked=this.onLookHistoryIsCLiked.bind(this);
    }


    render(){
        return(
            <div>
                <div id="dashboard-widget-last-top-up">
                    <ul className="inner-contents">
                        <li><h2 className="display-3">Factura</h2></li>
                        <li>
                                <ul className="inv-c-tab">
                                    <li><div className="icon icon-top-up-euro"></div></li>
                                    <li>
                                        <h3 className="txt-color-cta">La moment nu ai nici o factură </h3>
                                    </li>
                                </ul>
                        </li>
                        <li>
                            <button onClick={this.onLookHistoryIsCLiked}>Vezi istoricul facturilor</button>
                        </li>
                    </ul>
                </div>

            </div>
        );
    }
onLookHistoryIsCLiked(){
    console.log("Vezi istoricul facturilor");
}

}
