import React from 'react';
import { render } from 'react-dom';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import GaugeForm from "./GougeForm.jsx";

export default class MyResources extends React.Component{

    constructor(props){
        super(props);
        this.detailClicked=this.detailClicked.bind(this);
    }



    render(){

        return(
            <div className="balance-holder">
                <div id="dashboard-widget-balance">
                    <ul className="inner-contents">
                        <li>
                            <h2 className="display-3">Resursele mele</h2>
                            <span className="s_10"></span>
                            <h6>la data 28.11.2019 la 17:29</h6>
                        </li>
                        <li>
                            <span className="s_20"></span>
                            <ul className="bl-w">
                                <li>
                                    <div style={{textAlign:'center'}}>
                                        <div style={{minWidth: '120px', maxWidth: '130px', minHeight: '120px', maxHeight: '150px', margin: '0px auto', overflow: 'hidden'}}>
                                            <GaugeForm options={{value:22.3,unit:'GB'}}/>
                                        </div>
                                        <div id="gbCircleContent"></div>
                                        <h6 className="">Internet Mobil</h6>
                                    </div>
                                </li>
                                <li>
                                    <div style={{textAlign:'center'}}>
                                        <div style={{minWidth: '120px', maxWidth: '130px', minHeight: '120px', maxHeight: '150px', margin: '0px auto', overflow: 'hidden'}}>
                                            <GaugeForm options={{value:2300,unit:'min'}}/>
                                        </div>
                                        <div id="gbCircleContent"></div>
                                        <h6 className="">Internet Mobil</h6>
                                    </div>
                                </li>

                                <li>
                                    <div style={{textAlign:'center'}}>
                                        <div style={{minWidth: '120px', maxWidth: '130px', minHeight: '120px', maxHeight: '150px', margin: '0px auto', overflow: 'hidden'}}>
                                            <GaugeForm options={{value:2212,unit:'SMS'}}/>
                                        </div>
                                        <div id="gbCircleContent"></div>
                                        <h6 className="">Internet Mobil</h6>
                                    </div>
                                </li>                
                        </ul>
                        </li>
                        <li>
                            <span className="s_20"></span>
                            <button onClick={this.detailClicked}>Detalii </button>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }

    detailClicked(){
        console.log('details on my resources datat is clicked ');
    }
} 