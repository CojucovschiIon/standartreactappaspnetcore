import React from "react";

import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

export default class GaugeForm extends React.Component{

render(){
    return(
        <HighchartsReact  highcharts={Highcharts} options={{
            chart: {
                styleModel:true,
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                height:120,
                width:120,
            },
            title: {
                useHTML: true,
                text:
                    '<div style="width:60px;margin-top:30px;"><div style="text-align:center"><h6>'+this.props.options.value+' </h6><h6> '+this.props.options.unit+'</h6></div></div>',
                align: 'center',
                verticalAlign: 'middle',
                y:0
            },
            plotOptions: {
                pie: {
                    colors: ["#4BB4E6", "#eeeeee"],
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white'
                        }
                    },
                    startAngle: 0,
                    endAngle: 0,
                    center: ['50%', '50%'],
                    size: '120%'
                }
            },
            credits: {
                enabled: false,
            },
            tooltip: {
                enabled: false
            },
            series: [{
                type: 'pie',
                name: '',
                innerSize: '70%',
                data: [
                    ['', 1 ]
                ]
            }],
            exporting: { enabled: false }
        }}/>
    );
}


}