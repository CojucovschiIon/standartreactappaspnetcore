import  React from 'react';


export default class AccountBalance extends React.Component{


    constructor(props){
        super(props);
        this.onRechargeCliked=this.onRechargeCliked.bind(this);
    }

    onRechargeCliked(){
        console.log('REdirect to recharge apge !!!');
    }
    render(){
        return(
            <div>
                <div id="dashboard-widget-account-balance">
                    <ul className="inner-contents">
                        <li>
                                <h2 className="display-3">Balanţa contului</h2>
                        </li>
                        &nbsp;
                        <li>
                            <h3 className="txt-muted">0.00 MDL</h3>
                            <span className="s_15"></span>
                        </li>
                        &nbsp;
                        <li>
                            <button className="cta" onClick={this.onRechargeCliked}>Reîncarcă acum</button>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }

}

