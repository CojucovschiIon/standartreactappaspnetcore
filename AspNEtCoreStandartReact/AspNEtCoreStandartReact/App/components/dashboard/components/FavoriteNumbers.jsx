import React from 'react';

export default class FavoriteNumbers extends React.Component{

constructor(props)
{
    super(props);
    this.onGestionateClicking=this.onGestionateClicking.bind(this);
}

    render(){
        return(
            <div>
                <div id="dashboard-widget-favorite-number">
                    <ul className="inner-contents">
                        <li>
                            <h2 className="display-3">Numere Favorite</h2>
                        </li>
                        <li>
                            <ul className="favorite-number-list">
                                        <li>
                                            <div className="icon icon-unlimited-calls"></div>
                                            <h4>0XXXXXXXX</h4>
                                        </li>
                                        <li>
                                            <div className="icon icon-unlimited-calls"></div>
                                            <h4>0XXXXXXXX</h4>
                                        </li>
                                        <li>
                                            <div className="icon icon-unlimited-calls"></div>
                                            <h4>0XXXXXXXX</h4>
                                        </li>
                            </ul>
                        </li>
                        <li>
                            <button onClick={this.onGestionateClicking}>Gestionează numerele</button>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }

onGestionateClicking(){
    console.log("Details is cliked");
}

}
