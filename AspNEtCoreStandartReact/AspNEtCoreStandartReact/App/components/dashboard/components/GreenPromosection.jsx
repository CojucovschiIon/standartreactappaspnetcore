import React from 'react';

export default class GreenPromoSection extends React.Component{

    constructor(props){
        super(props);
        this.onVisitClick=this.onVisitClick.bind(this);
    }
    onVisitClick(){
        console.log('Visit to do');
    }
    render(){
        return(
            <section id="promo-section">
                <div className="gridContainer clearfix">
                    <ul className="promo-contents">
                        <li>
                            <h3 className="display-3">Beneficiază de oferte exclusive</h3>
                            <span className="s_20"></span>
                            <button onClick={this.onVisitClick}>Vizitează Magazinul online</button>
                        </li>
                        <li><img src="/Content/assets/img/promo-section/rocket.png" alt="rocket"/></li>
                    </ul>
                </div>
            </section>
        );
    }
}
