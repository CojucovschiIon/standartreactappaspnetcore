import React from 'react';


export default class OptionDeactivate extends React.Component{
    render(){
        return(
            <ul>
                <li>
                    <span className="s_10"></span>
                    <h6>Roaming</h6>
                    <h6 className="txt-color-positive">Activat</h6>
                </li>
                <li>
                    <span className="s_10"></span>
                    <p></p>
                    <div className="button button-disabled" data-popup="#" data-popup-tmpl="local_deactivate_dialog" data-option-id="000027120668_web-technical-info_roaming" data-option-name="Roaming" data-option-isservice="yes" data-option-form-id="option_form_000027120668_web-technical-info_roaming">
                        Dezactivează
                    </div>
                </li>
            </ul>
        );
    }
}


