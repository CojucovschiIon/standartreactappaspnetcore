import React from 'react';


import { connect } from 'react-redux';
import { showActivationOptionPopup, closePopup} from '../../actions/index.jsx';



class OptionsDescriptions extends React.Component{

    constructor(props){
        super(props);
        this.onActivateIsClicked=this.onActivateIsClicked.bind(this);

    }



    onActivateIsClicked(){
        console.log("activate is cliked !!!!");
       this.props.showActivationOptionPopup({Show:true});
    }


    render(){
        return(
            <div className="accordion-content " data-role="accordion-content-container">
                <div className="inner">
                    <ul className="list">
                        <li className="list-item type-b">
                            <ul>
                                <li>
                                    <h6 className="txt-color">50 MB Bonus Zilnic</h6>
                                    <h6 className="txt-color-cta">Gratuit </h6>
                                </li>
                                <li>
                                    <span className="s_5"></span>
                                        <p>
                                            <span style={{whiteSpace:'pre-line'}}>50 MB trafic Internet Mobil gratuit pentru toţi utilizatorii My Orange

(!) Accesează zilnic My</span>
                                            <span className="a txt-style-u txt-style-b" data-role="expand-txt" style={{whiteSpace:'pre-line'}}>
                                                Mai detaliat
                                            </span>
                                            <span className="expand-txt" style={{whiteSpace:'pre-line'}}> Orange şi activează gratuit bonusul în fiecare zi

Valabilitate: până la 23:59</span>
                                        </p>
                                </li>
                            </ul>

                            <div className="button cta" onClick={this.onActivateIsClicked}> 
                                Activează
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
                            
        );
    }
}


function mapStateToProps(){
    return{

    }
}

export default connect(mapStateToProps,{showActivationOptionPopup,closePopup})(OptionsDescriptions)
