import React from 'react';
import OptionsDescription from './OptionsDescription.jsx';


export default class OptionsCategory extends React.Component{
    render(){
        return(
            <div className="accordion-item-a" data-role="accordion-container">
                        <div className="accordion-header-a" data-role="accordion-trigger">
                            <ul>
                                <li><span className="icon icon-internet-usage"></span></li>
                                <li>
                                    <h6>Internet Mobil</h6>
                                    <span className="s_5"></span>
                                </li>
                                <li><span className="icon icon-arrow-down"></span></li>
                            </ul>
                        </div>
                        <OptionsDescription/>    
                        <OptionsDescription/>    
                        <OptionsDescription/>    
                        <OptionsDescription/>    
                        <OptionsDescription/>    
                        <OptionsDescription/>    
            </div>
        );
    }
}


