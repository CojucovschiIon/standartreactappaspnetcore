import React from "react";

import { connect } from 'react-redux';
import { showActivationOptionPopup, closePopup} from '../../actions/index.jsx';


import Popup from "./Popup.jsx";

class PopupManager extends React.Component{
    render(){
        return(
            <div>
                {this.props.PopupModel.Show ? <Popup/> : <div></div>}
            </div>

        );
    }
}

function mapStateToProps(state){
    //console.log("POP_upmanager ",state);
    return{
      PopupModel:state.popupReducer.PopupModel
    }
}

export default connect(mapStateToProps,{showActivationOptionPopup,closePopup})(PopupManager);
