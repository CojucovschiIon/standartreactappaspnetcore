import React from 'react'


import { connect } from 'react-redux';
import { showResponseMessagePopup, closePopup} from '../../../actions/index.jsx';
import { delay } from 'q';

class OptionActivateTemplate extends React.Component{

    constructor(props){
        super(props);
        this.onCancelClicked=this.onCancelClicked.bind(this);
        this.onAcceptIsClicked=this.onAcceptIsClicked.bind(this);
    }

    onCancelClicked(){
        this.props.closePopup({Show:true});
    }

    onAcceptIsClicked(){
        
        delay(3000);
        this.props.showResponseMessagePopup({Show:true});

    }

    render(){
        return(
            <div>
                <div className="popup-content short active"><div className="popup-close" onClick={this.onCancelClicked}>×</div>
                    <div className="inner">
                    <h2 className="data-option-action">Confirmare</h2>
                    <span className="s_10 dialog-description"></span>
                    <strong className="data-option-description txt-xl-size">Doreşti să continui activarea opţiunii?</strong>
                    <span className="s_20"></span>
                    <ul className="tab-d theme-a">
                        <li>
                            <strong className=".optin-title data-option-name txt-xl-size">50 MB Bonus Zilnic</strong>
                        </li>
                        <li>
                            <strong className="txt-color-cta data-option-price txt-xl-size">Gratuit </strong>
                        </li>
                    </ul>
                    <span className="s_20"></span>
                    <ul className="form-cta-holder">
                        <li>
                            <div className="inline button" onClick={this.onCancelClicked}>Anulează</div>
                            &nbsp;&nbsp;
                            <div className="inline button cta" id="btn-dialog-add-option" onClick={this.onAcceptIsClicked}>Activează</div>
                        </li>
                    </ul>
                    </div></div>
                <div className="popup-overlay"></div>
            </div>
        );
    }
}

function mapStateToProps(state){
    return{
        
    }
}

export default connect(mapStateToProps,{showResponseMessagePopup, closePopup})(OptionActivateTemplate);
