import React from 'react';



import { connect } from 'react-redux';
import { showActivationOptionPopup, closePopup} from '../../../actions/index.jsx';

class ResponseMessageTemplate extends React.Component{

    constructor(props){
        super(props);
        this.onCancelClicked=this.onCancelClicked.bind(this);
    }

    onCancelClicked(){
        this.props.closePopup({Show:true});
    }



    render(){
        return(
            <div>
                <div className="popup-content short active"><div className="popup-close" onClick={this.onCancelClicked}>×</div><div className="inner">
                <div style={{textAlign:'center'}}>
                    <span className="display-1 data-dialog-ico icon icon-Warning-important txt-color-digital_func_yellow"></span>
                    <span className="s_20"></span>
                    <h6 className="data-dialog-message">Această opțiune este deja activată</h6>
                    <span className="s_20"></span>
                    <span className="s_10"></span>
                    <div id="close-button-for-local-mesagedialog" className="inline button" onClick={this.onCancelClicked}>Închide</div>
                </div>
            </div></div><div className="popup-overlay"></div></div>
        );
    }
}


function mapStateToProps(state){
    return{
        
    }
}

export default  connect(mapStateToProps,{closePopup})(ResponseMessageTemplate)
