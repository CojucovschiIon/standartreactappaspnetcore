import React from 'react';


import { connect } from 'react-redux';
import { showActivationOptionPopup, closePopup} from '../../actions/index.jsx';

import OptionActivateTemplate from './popuptemplate/OptionActivateTemplate.jsx';
import ResponseMessageTemplate from './popuptemplate/ResponseMessageTemplate.jsx';

class Popup extends React.Component{
   
    constructor(props){
        super(props);
        this.renderPopupForm=this.renderPopupForm.bind(this);

    }
renderPopupForm(PopupModel)
{
    switch(PopupModel.Template)
    {   
        case "option_activate":
                return <OptionActivateTemplate />
        case "response_mesage":
                console.log("render popup")
                return <ResponseMessageTemplate />
        default : return <div></div>;
    }
}


    render(){

        return(
            <div id="modal-box">
                {this.renderPopupForm(this.props.PopupModel)}
            </div>
        );
    }
}



function mapStateToProps(state){
    ///console.log("POP_upmanager ",state);
    return{
        PopupModel:state.popupReducer.PopupModel
    }
}

export default connect(mapStateToProps,{showActivationOptionPopup,closePopup})(Popup)
