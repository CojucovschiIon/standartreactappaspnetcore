import React from 'react';
import AccountBalance from "./component/AccountBalance.jsx";
import AccordionItem from "./component/AccordionItem.jsx";

export default class ResourcesContainer extends React.Component{


render(){
    return(
        <div className="gridContainer clearfix">
            <div className="s_40"></div>
            <AccountBalance />
            <div className="s_40"></div>
            <div className="accordion-container">
                <AccordionItem />
                <AccordionItem />
                <span className="s_20"></span>
                <div className="list-item">
                    <button onClick={()=>{document.location.href='/optionsandservices/optionsservicelist'}}>Activează o opţiune</button>
                </div>
            </div>
        </div>





    );
}



}

