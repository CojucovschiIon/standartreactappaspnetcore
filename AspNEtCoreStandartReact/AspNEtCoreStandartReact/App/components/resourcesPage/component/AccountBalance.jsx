import React from "react";


export default class AccountBalance extends React.Component{
    
    constructor(props){
        super(props);
        this.onRechargeClick=this.onRechargeClick.bind(this);
    }
    
    onRechargeClick(){
       window.location.href='/payment/topup';
    }
    
    render(){
        return(
            <div className="flex-container">
                <div className="flex between">
                    <div className="margin_0_20_0_0">
                        <h5>Balanţa contului</h5>
                        <span className="s_5"></span>
                        <h3 className="txt-muted">0.00 MDL</h3>

                    </div>
                    <div>
                        <span className="s_5"></span>
                        <button className="cta" onClick={this.onRechargeClick}>Reîncarcă acum</button>
                    </div>

                </div>
            </div>

        );

    }
}

