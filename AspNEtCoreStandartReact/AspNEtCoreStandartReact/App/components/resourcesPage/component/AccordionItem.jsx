import React from 'react';


export default class AccordionItem extends React.Component{



    render(){
        return(
            <div className="accordion-item active">
                <div className="accordion-header flex-container">
                    <img src="/Content/assets/img/subscriptions-icons/icon_plan.png" className="img-icon" alt="Abonament"/>
                    <span>Inclus în abonament</span>
                </div>
                <div className="accordion-contents" style={{display: 'block'}}>
                    <div className="flex-container">
                                    <div className="w-100">
                                        <div className="inner-item">
                                            <div data-scope="xm-title">Trafic Internet din abonament</div>
                                            <div data-scope="xm-title" className="txt-color-cta">21.42 GB</div>
                                        </div>
                                        <div className="s_5"></div>
                                        <div className="range-slider-controls nomargin resources_style">
                                            <progress value="100" max="0" className="green-bar"></progress>
                                            <div className="range-scale-bottom">
                                                <div>0</div>
                                                <div>21.42 GB</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="w-100">
                                        <div className="inner-item">
                                            <div data-scope="xm-title">Minute naţionale din abonament</div>
                                            <div data-scope="xm-title" className="txt-color-cta">1399 min 29 sec</div>
                                        </div>
                                        <div className="s_5"></div>
                                        <div className="range-slider-controls nomargin resources_style">
                                            <progress value="100" max="0" className="green-bar"></progress>
                                            <div className="range-scale-bottom">
                                                <div>0</div>
                                                <div>1399 min 29 sec</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="w-100">
                                        <div className="inner-item">
                                            <div data-scope="xm-title">SMS din abonament</div>
                                            <div data-scope="xm-title" className="txt-color-cta">1386 SMS</div>
                                        </div>
                                        <div className="s_5"></div>
                                        <div className="range-slider-controls nomargin resources_style">
                                            <progress value="100" max="0" className="green-bar"></progress>
                                            <div className="range-scale-bottom">
                                                <div>0</div>
                                                <div>1386 SMS</div>
                                            </div>
                                        </div>
                                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

