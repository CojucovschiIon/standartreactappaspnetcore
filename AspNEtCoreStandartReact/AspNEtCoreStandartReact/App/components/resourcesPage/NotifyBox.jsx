import React from 'react';
import { extend } from 'highcharts';


export default class NotifyBox extends React.Component{


render(){
    return(
        <section id="notify-c-box">
            <div className="gridContainer clearfix">
                <div className="flex-container notify-box">
                    <div>
                        <div className="inner-item">
                            <div data-scope="title">
                                Resursele mele la data 29.11.2019 la  14:29
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}


}
