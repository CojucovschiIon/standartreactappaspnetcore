import React from "react";


export default class OptionDescription extends React.Component{
    constructor(props){
        super(props);
        this.onActivationIsCliked=this.onActivationIsCliked.bind(this);
    }

    onActivationIsCliked(){
        console.log("TODO:   Activation option is cliked !!!!")
    }
    render(){
        return(
            
            <ul>
                <li>
                    <h6>Bonus 1 GB pentru reîncărcare online</h6>
                </li>
                <li>
                    <span className="s_10"></span>
                        <p>
                            Acum, fiecare reîncărcare online a contului Orange de minim 100 lei vă aduce un bonus de 1 GB
                            <span className="a txt-style-u txt-style-b" data-role="expand-txt" style={{whiteSpace:'pre-line'}}>
                                Mai detaliat
                            </span>
                            <span className="expand-txt" style={{whiteSpace:'pre-line'}}>
                                trafic Internet, disponibil 24 ore. Bonusul se va aloca automat, la finalizarea cu succes a tranzacției, pe numărul indicat în procesul de reîncărcare.
                            </span>
                        </p>

                    <span className="s_10"></span>
                                    <div className="button cta" onClick={this.onActivationIsCliked}>
                                        Reîncarcă acum
                                    </div>
                    <span className="s_10"></span>
                    <hr/>
                </li>
            </ul>

        );
    }


}


