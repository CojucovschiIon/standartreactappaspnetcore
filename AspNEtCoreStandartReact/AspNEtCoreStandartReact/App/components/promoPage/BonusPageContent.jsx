import React from 'react';
import OptionDescription from './components/OptionDescription.jsx';



export default class BonusPageContent extends React.Component{
    render(){
        return(
            <div className="gridContainer clearfix">
            <div className="section-inner">
                    <span className="s_20"></span>
                    <ul className="list">
                            <li className="list-item type-a">
                                <OptionDescription/>
                            </li>
                            <li className="list-item type-a">
                                <OptionDescription/>
                            </li>
                            <li className="list-item type-a">
                                <OptionDescription/>
                            </li>
                    </ul>
                </div>
            </div>
        );
    }
}
