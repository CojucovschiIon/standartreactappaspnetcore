import React from 'react';


export default class BonusHeader extends React.Component{
    render(){
        return(
            <div id="bonus-page-header">
                <div className="gridContainer clearfix">
                    <div className="inner">

                        <div className="flex-container">

                            <div>
                                <h2 className="m">
                                    Mulţumim că foloseşti My Orange!<br/>
                                    Special pentru tine am creat o serie de bonusuri și oferte
                                </h2>
                                <h4 className="dt">
                                    Mulţumim că foloseşti My Orange!<br/>
                                    Special pentru tine am creat o serie de bonusuri și oferte
                                </h4>
                                <span className="s_40"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
