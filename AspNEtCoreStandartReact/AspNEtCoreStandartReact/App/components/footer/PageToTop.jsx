import React from 'react';


export default class PageToTop extends React.Component{
    render(){
        return(
            <section id="page-to-top" className="last-animation contentEffect" style={{display: 'block'}}>
                <div className="gridContainer clearfix">
                    <div className="to-top-bt">
                        <i className="fa fa-chevron-up" aria-hidden="true"></i>
                        <span>Sus</span>
                    </div>
                </div>
            </section>
        );
    }
}
