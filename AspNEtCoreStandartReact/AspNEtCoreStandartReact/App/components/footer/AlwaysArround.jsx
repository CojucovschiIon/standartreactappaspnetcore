import React from 'react';





export default class AlwaysArround extends React.Component{

constructor(props){
    super(props);
    this.onFrequentQuestionsClick=this.onFrequentQuestionsClick.bind(this);
    this.onEshopClicked=this.onEshopClicked.bind(this);
    this.onOrangeShopsClick=this.onOrangeShopsClick.bind(this);
    this.onSupportClick=this.onSupportClick.bind(this);
}




onFrequentQuestionsClick(){
    window.location.href='https://www.orange.md/?l=1&amp;p=1&amp;c=7&amp;sc=700';
}
onEshopClicked(){
    window.location.href='https://eshop.orange.md/ro/?l=1&amp;utm_source=Contul_meu_RO&amp;utm_medium=link';
}
onSupportClick(){
    window.location.href='https://proxychat.orange.md/?dnis=OrangeMD&amp;lang=ro';
}
onOrangeShopsClick(){
    window.location.href='https://www.google.com/maps/search/orange/';
}

    render(){
    return(
        <section id="always-around" className="last-animation contentEffect" style={{display: 'block'}}>
            <div className="gridContainer clearfix">
                <div className="div1_1_1_1">
                    <h2 className="display-3">Suntem întotdeauna aproape</h2>
                </div>
                <div className="s_40"></div>
                <div className="div18_4_1_1 no-break">
                    <div className="inner-content" style={{cursor: 'pointer'}} onClick={this.onFrequentQuestionsClick}>
                        <em className="icon icon-assistance"></em>
                        <h3>Întrebări frecvente</h3>
                    </div>
                </div>
                <div className="div18_4_2_1 no-break">
                    <div className="inner-content" style={{cursor: 'pointer'}} onClick={this.onOrangeShopsClick}>
                        <em className="icon icon-my-boutique"></em>
                        <h3>Magazine Orange</h3>
                    </div>
                </div>
                <div className="div18_4_3_1 no-break">
                    <div className="inner-content" style={{cursor: 'pointer'}} onClick={this.onSupportClick}>
                        <em className="icon icon-control-centre"></em>
                        <h3>Serviciul Suport Clienţi</h3>
                    </div>
                </div>
                <div className="div18_4_4_1 no-break">
                    <div className="inner-content" style={{cursor: 'pointer'}} onClick={this.onEshopClicked}>
                        <em className="icon icon-buy"></em>
                        <h3>Magazin online</h3>
                    </div>
                </div>
            </div>
        </section>
    );
}



}
