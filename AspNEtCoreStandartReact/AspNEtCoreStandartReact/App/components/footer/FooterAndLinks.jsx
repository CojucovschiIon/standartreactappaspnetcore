import React from 'react';


export default class FooterAndLinks extends React.Component{
    render(){
        return(
            <footer className="last-animation contentEffect" style={{display: 'block'}}>
                <section id="follow-links" className="noselect">
                    <div className="col-xs-12">
                        <div className="row" style={{borderTop:'1px solid #232323',borderBottom:'1px solid #232323',paddingBottom:'0px',paddingTop: '20px',marginBottom:'0px'}}>
                            <div className="container social_icons">
                                <div className="row" style={{marginBottom:'0px'}}>
                                    <div className="col-xs-12 col-sm-2 col-lg-2">
                                        <div className="social_icons_footer text-left first" style={{paddingRight:'20px',fontSize: '14px',color: '#FFF', lineHeight: '36px',whiteSpace: 'nowrap'}}>Urmăriți-ne:</div>
                                    </div>
                                    <div className="col-xs-12 col-sm-10">
                                        <div className="row" style={{marginBottom:'0px'}}>
                                            <div className="col-xs-12 col-sm-4 col-lg-3" style={{marginBottom:'20px'}}>
                                                <div className="row" style={{marginBottom:'0px'}}>
                                                    <div className="col-xs-3 text-center">
                                                        <div className="social_icons_footer facebook"><a href="//www.facebook.com/Orange.Moldova"><em className="fa fa-facebook"> </em></a></div>
                                                    </div>
                                                    <div className="col-xs-3 text-center">
                                                        <div className="social_icons_footer twitter"><a href="//twitter.com/orangemd/"><em className="fa fa-twitter"> </em></a></div>
                                                    </div>
                                                    <div className="col-xs-3 text-center">
                                                        <div className="social_icons_footer linkedin"><a href="//www.linkedin.com/company/orange/"><em className="fa fa-linkedin"> </em></a></div>
                                                    </div>
                                                    <div className="col-xs-3 text-center">
                                                        <div className="social_icons_footer youtube"><a href="//www.youtube.com/user/orangemdofficial"><em className="fa fa-youtube"> </em></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xs-12 col-sm-4 col-lg-3" style={{marginBottom:'20px'}}>
                                                <div className="row" style={{marginBottom:'0px'}}>
                                                    <div className="col-xs-3 text-center">
                                                        <div className="social_icons_footer instagram"><a href="//instagram.com/orangemoldova"><em className="fa fa-instagram"> </em></a></div>
                                                    </div>
                                                    <div className="col-xs-3 text-center">
                                                        <div className="social_icons_footer odnoklassniki"><a href="//odnoklassniki.ru/group/52971409965182"><em className="fa fa-odnoklassniki"> </em></a></div>
                                                    </div>
                                                    <div className="col-xs-3 text-center">
                                                        <div className="social_icons_footer telegram"><a href="//t.me/OrangeMoldova"><em className="fa fa-paper-plane"> </em></a></div>
                                                    </div>
                                                    <div className="col-xs-3 text-center">
                                                        <div id="mail" className="social_icons_footer mail"><a href="https://www.orange.md/?l=1&amp;p=1&amp;c=12&amp;sc=1"><em className="fa fa-envelope" aria-hidden="true"> </em></a></div>
                                                    </div>
            
                                                </div>
                                            </div>
                                            <div className="col-xs-12 col-sm-4 col-lg-3" style={{marginBottom:'20px'}}>
                                                <div className="row" style={{marginBottom:'0px'}}>
                                                    <style dangerouslySetInnerHTML = {{__html:` 
                                                    #subscribe, #unsubscribe {
                                                            display: none;
                                                        }`}} /> 
                                                    <div id="subscribe_container" className="col-xs-3 text-left">
                                                        <div id="subscribe" className="social_icons_footer odnoklassniki" style={{display:'inline-block'}}><a title="Abonează-te la notificări în browser" href="#"><em className="fa fa-bell"> </em></a></div>
                                                        <div id="unsubscribe" className="social_icons_footer odnoklassniki" style={{display: 'none'}}><a title="Dezabonează-te la notificări din browser" href="#"><em className="fa fa-bell-slash" aria-hidden="true"> </em></a></div>
                                                    </div>
                                                    <div className="col-xs-3 text-center"></div>
                                                    <div className="col-xs-3 text-center"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="footer-links">
                    <div style={{borderBottom:'0px solid #232323',marginBottom:'0px'}}>
                        <div className="container ftr">
                            <div className="row" style={{marginBottom:'0px'}}>
                                <div className="col-xs-12 col-sm-4 col-md-3 col-lg-3" style={{marginBottom:'15px'}}>
                                    <h4><a className="hdr-H5 toogle_link white tab amenu collapsed" role="button" data-toggle="collapse" href="#collapseListGroup1" aria-expanded="false" aria-controls="collapseListGroup1">Util</a></h4>
                                    <div className="spacer10"></div>
                                    <div id="collapseListGroup1" className="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseListGroupHeading1" aria-expanded="true">
                                        <ul className="list-unstyled">
                                            <li> <a href="https://www.orange.md/ro/util/protejeaza-te-de-fraude">Protejează-te de fraude</a></li>
                                            <li> <a href="https://www.orange.md/?l=1&amp;p=1&amp;c=8&amp;sc=87">Semnătura Mobilă</a></li>
                                            <li><a href="https://www.orange.md/?l=1&amp;p=1&amp;c=1&amp;sc=6">Cod de etică</a></li>
                                            <li><a href="https://www.orange.md/?l=1&amp;p=1&amp;c=1&amp;sc=1">Cariera</a></li>
                                            <li><a href="https://www.google.com/maps/search/orange/">Magazine</a></li>
                                            <li><a href="https://www.orange.md/?l=1&amp;p=1&amp;c=7&amp;sc=77">Condiţii contractuale</a></li>
                                            <li><a href="https://www.orange.md/?l=1&amp;p=1&amp;c=7&amp;sc=771">Lista cererilor</a></li>
                                            <li><a href="https://www.orange.md/?l=1&amp;p=1&amp;c=4&amp;sc=44">Documente necesare</a></li>
                                            <li><a href="https://www.orange.md/?l=1&amp;p=1&amp;c=8&amp;sc=85">ISO</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-4 col-md-3 col-lg-3" style={{marginBottom:'15px'}}>
                                    <h4><a className="hdr-H5 toogle_link white tab amenu collapsed" role="button" data-toggle="collapse" href="#collapseListGroup3" aria-expanded="false" aria-controls="collapseListGroup3">Pagini web</a></h4>
                                    <div className="spacer10"></div>
                                    <div id="collapseListGroup3" className="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseListGroupHeading3" aria-expanded="true">
                                        <ul className="list-unstyled">
                                            <li><a href="http://csr.orange.md">csr.orange.md</a></li>
                                            <li><a href="https://eshop.orange.md/">eshop.orange.md</a></li>
                                            <li><a href="https://orangetext.md/">orangetext.md</a></li>
                                            <li><a href="https://help.orange.md/">help.orange.md</a></li>
                                            <li><a href="https://fundatia.orange.md/">fundatia.orange.md</a></li>
                                            <li><a href="http://speedtest.orange.md/">speedtest.orange.md</a></li>
                                            <li><a deep="DEEP_HOME" href="https://www.orange.md/app/home/start/c81e728d9d4c2f636f067f89cc14862c/">my.orange.md</a></li>
                                            <li><a href="http://www.orange.com" target="_blank" rel="noopener noreferrer">orange.com</a></li>
                                            <li><a href="https://cybersecurity.orange.md" target="_blank" rel="noopener noreferrer">cybersecurity.orange.md</a></li>
                                            <li><a href="http://ajutor.orange.md" target="_blank" rel="noopener noreferrer">ajutor.orange.md</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3" style={{marginBottom:'15px'}}>
                                    <h4><a className="hdr-H5 toogle_link white tab amenu collapsed" role="button" data-toggle="collapse" href="#collapseListGroup2" aria-expanded="false" aria-controls="collapseListGroup2">Informaţii</a></h4>
                                    <div className="spacer10"></div>
                                    <div id="collapseListGroup2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseListGroupHeading2" aria-expanded="true">
                                        <ul className="list-unstyled">
                                            <li><a href="https://www.orange.md/files/securitatea_datelor.pdf" target="_blank" rel="noopener noreferrer">Securitatea datelor</a></li>
                                            <li><a href="https://www.orange.md/?l=1&amp;p=2&amp;c=2&amp;sc=25">Interconectare şi acces</a></li>
                                            <li> <a href="https://www.orange.md/ro/achizitii-responsabile">Achizitii Responsabile</a></li>
                                            <li><div className="spacer10"></div></li>
                                            <li> <a style={{lineHeight: 'normal'}} href="https://www.orange.md/ro/pdf/1543454224665830">Termeni şi Condiţii generale aplicabile comenzilor Orange</a></li>
                                            <li><div className="spacer10"></div></li>
                                            <li> <a href="https://www.orange.md/ro/cum-depui-o-reclamatie">Cum depui o reclamaţie</a></li>
                                            <li><div className="spacer10"></div></li>
                                            <li> <a style={{lineHeight: 'normal'}} href="https://www.orange.md/?l=1&amp;p=1&amp;c=8&amp;sc=802">Notă cu privire la colectarea de fonduri</a></li>
                                            <li><div className="spacer10"></div></li>
                                            <li> <a href="https://www.orange.md/ro/alte-informatii">Alte informaţii</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-4 col-md-3 col-lg-3" style={{marginBottom:'15px'}}>
                                    <h4><a className="hdr-H5 toogle_link white tab amenu collapsed" role="button" data-toggle="collapse" href="#collapseListGroup4" aria-expanded="false" aria-controls="collapseListGroup4">Suport</a></h4>
                                    <div className="spacer10"></div>
                                    <div id="collapseListGroup4" className="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseListGroupHeading4" aria-expanded="true">
                                        <ul className="list-unstyled">
                                            <li><a deep="DEEP_HOME" href="https://www.orange.md/app/home/start/c81e728d9d4c2f636f067f89cc14862c/">Contul meu</a></li>
                                            <li><a href="https://help.orange.md/">Forum</a></li>
                                            <li><a href="http://chat.orange.md">Chat</a></li>
                                            <li><a href="https://www.orange.md/?l=1&amp;p=1&amp;c=10&amp;sc=114">Acoperire rețea</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <style dangerouslySetInnerHTML = {{__html:`
                    #footer-terms-container {
                        border-top: 1px solid #232323;
                        padding-top: 20px;
                        padding-bottom: 20px;
                        background-color: #000000;
                        width: 100%;
                        margin: 0 auto;
                        text-align: center;
                    }
            
                        #footer-terms-container a {
                            color: #ff7900;
                        }
                `}}></style>
                <div id="footer-terms-container">
                    <a target="_blank" href="/Content/assets/pdf/Termeni si Conditii_De utilizare a seriviciului My Orange_RO.pdf">
                        Termeni şi Condiţii
                    </a>
                </div>
            </footer>


        );
    }
}

