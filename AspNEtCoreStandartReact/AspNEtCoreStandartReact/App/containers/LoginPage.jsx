﻿import React from 'react';
import { connect } from 'react-redux';
import { setDexLanguage, setCurrentLanguage, autologinUser } from '../actions/index.jsx';



class LoginPage extends React.Component {

    constructor(props) {
        super(props);
        this.OnchangeInput = this.OnchangeInput.bind(this);
        this.OnLoginCliked = this.OnLoginCliked.bind(this);

    }


    OnchangeInput = () => {


    }
    OnLoginCliked = (e) => {
        e.preventDefault();
        fetch("https://third-diagram-179021.firebaseio.com/User.json").then(res => res.json()).then(resp => {
            console.log(resp);
            this.props.autologinUser(resp);
        });
    }

    render() {
        return (
            <div id="login">
                <section>
                    <div className="gridContainer clearfix">
                        <div className="s_60"/>
                        <div className="div17_2_1_1">
                            <div className="wbg " style={{ display: 'block', width: '100%', float: 'left', padding: '40px' }}>
                                <h1>My Orange</h1>
                                <form className="_animated _slideInU"
                                    onSubmit={this.OnLoginCliked}
                                    id="identify-form" method="post">
                                    <span className="s_20"/>
                                    <span className="s_20"/>
                                    <strong className="txt-m-size">Numărul Orange</strong>
                                    <span className="s_10"/>
                                    <input aria-describedby=""
                                        value="60031400"
                                        onChange={this.OnchangeInput}
                                        aria-invalid="false"
                                        className="numbers-only phone-login"
                                        data-msg="Introdu numărul Orange "
                                        data-msg-regxlogin="Formatul numărului introdus este incorect."
                                        id="SendTextValue"
                                        maxLength="9"
                                        name="SendTextValue" placeholder="0XXXXXXXX"
                                        type="tel" value="" width="100%" />
                                        <span className="s_20"/>
                                        <div className="m">
                                            <button type="submit" className="stretch cta">Continuă</button>
                                            <span className="s_10"/>
                                        </div>
                                        <div className="dt">
                                            <button type="submit" className="cta">Continuă</button>
                                        </div>
                                        <span className="s_15"/>
                                        <div>
                                            <a href="/login/redirecttomyorangecid" target="_blank" style={{ color: '#ff7900 !important' }}>Logare cu Cartela Clientului</a>
                                        </div>
                                        <div>
                                            <span className="s_40"/>
                                            <hr className="st"/>
                                            <span className="secured-msg">Te afli în zonă securizată</span>
                                        </div>
                                        <input name="__RequestVerificationToken" type="hidden" value="CfDJ8OM_3Y5Lg1tKh8aW68U1toAUtblP2t7Rmzr4JWys351TDOwd-aJDdeTEBBB6ff5xhujXNMrImJTvEgHqQF7RC0XEcdOrUd2h-dmpSRBVsw9Lrpn38FPwHWxBS5SX519Jn1NUtL4mgV3pmN1iw6Fwj4c" />
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="s_60"/>
            </div>
            );
    }



}





function mapStateToProps(state) {
    console.log(state);
    return {
        languageDex: state.languageDictionary,
        currentLanguage: state.languageDexReducer,
        User: state.userReducer.User


    }
}

export default connect(mapStateToProps, { autologinUser, setDexLanguage, setCurrentLanguage })(LoginPage);


