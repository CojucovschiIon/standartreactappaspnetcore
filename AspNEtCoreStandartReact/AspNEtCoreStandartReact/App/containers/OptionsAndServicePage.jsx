import React from 'react';

import {Link } from 'react-router-dom';
import OptionCategory from '../components/optionsAndServices/OptionsCategory.jsx';


export default class OptionsAndServicesPage extends React.Component{

    constructor(props){
        super(props);
        this.onActivateTabIsClicked=this.onActivateTabIsClicked.bind(this);
        this.onDeactivateTabIsClicked=this.onDeactivateTabIsClicked.bind(this);
    }

    onActivateTabIsClicked(){
        console.log("Activate Tab is Cliked !!!");
    }
    onDeactivateTabIsClicked(){
        console.log("Deactivate Tab is Cliked !!!");
    }
    render(){
        return(
            <section id="options-n-services">
                <div className="m">
                    <ul className="tab-c">
                        <li className="active" onClick={this.onActivateTabIsClicked}>
                            <span>Activează</span>
                        </li>
                        <li onClick={this.onDeactivateTabIsClicked}>
                            <span>Dezactivează</span>
                        </li>
                    </ul>
                </div>
                <div className="gridContainer clearfix special-m-width">
                    <div className="section-inner">
                        <div className="dt">
                            <span className="s_20"></span>
                            <ul className="tab-b">
                                <li className="active">
                                    <span>Activează</span>
                                </li>
                                <li>
                                    <span>
                                        <Link to ="/optionsandservices/manageoptionsandservices">Dezactivează</Link>
                                    </span>
                                </li>
                            </ul>
                            <span className="s_10"></span>
                        </div>
                        
                        <div className="tab-item">
                             <OptionCategory />  
                             <OptionCategory />  
                             <OptionCategory />  
                             <OptionCategory />  
                             <OptionCategory />  
                             <OptionCategory />  
                             <OptionCategory />  
                             <OptionCategory />  
                             <OptionCategory />  
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
