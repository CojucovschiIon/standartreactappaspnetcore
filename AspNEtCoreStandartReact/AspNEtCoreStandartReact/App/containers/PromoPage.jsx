import React from 'react';
import { connect } from 'react-redux';

import BonusHeader from '../components/promoPage/BonusHeader.jsx';
import BonusPageContent from '../components/promoPage/BonusPageContent.jsx';
import {setCurrentPageName} from '../actions/index.jsx';



class PromoPage extends React.Component{

    render(){
        this.props.setCurrentPageName(this.props.currentLanguage["Promo.PromoPageName"]);
        return(
            <section id="bonus-n-special-offers">
                <BonusHeader/>
                <BonusPageContent/>
            </section>
        );
    }
}


function mapStateToProps(state){
    return {
        currentLanguage:state.languageDexReducer.CurrentLanguage
    }
}

export default connect(mapStateToProps,{setCurrentPageName})(PromoPage);

