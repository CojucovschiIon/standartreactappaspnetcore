import React from 'react';
import Banner from "../components/dashboard/Banner.jsx";
import DasboardMainContainer from "../components/dashboard/DashboardMainContainer.jsx";
import GreenPromoSection from "../components/dashboard/components/GreenPromosection.jsx";

export default class DasboardPage extends React.Component{

render(){
    return(
        <div id='dashboard'>
            <section>
            <div className="gridContainer clearfix">
                <Banner/>
                <DasboardMainContainer />
            </div>
            </section>
            
            <div className="s_60"></div>
            <GreenPromoSection />
        </div>
    )
}



}

