import React from 'react';
import { connect } from 'react-redux';
import { setDexLanguage,setCurrentLanguage, setCurrentPageName} from '../actions/index.jsx';

import MagicNumberContent from '../components/favoritenumbers/MagicNumberContent.jsx';
import MagicNumberExperience from '../components/favoritenumbers/MagicNumberExperience.jsx';


class FavorireNumberPage extends React.Component{




    render(){
        this.props.setCurrentPageName(this.props.currentLanguage["MagicNumber.MagicNumberPageName"]);

        return(
            <section id='magic-number'>
                <div className='gridContainer clearfix'>
                    <div className="d-item">
                        <div className="m">
                            <div className="flex">
                                <div className="w-100">
                                    <span className="s_20"></span>
                                        <strong className="txt-xl-size update_description_mn">{this.props.currentLanguage["MagicNumber.Description"]}</strong>
                                    <span className="s_20"></span>
                                </div>
                            </div>
                        </div>
                        <div className="dt">
                            <div className="flex ">
                                <div className="w-75">
                                    <span className="s_40"></span>
                                    <strong className="txt-xl-size update_description_mn">{this.props.currentLanguage["MagicNumber.Description"]}</strong>
                                    <span className="s_40"></span>
                                </div>
                                <div className="w-25">
                                    <div style={{textAlign:'center'}}>
                                        <img className="d-item-img" alt="" src="/Content/assets/img/magic_number.gif" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div className="section-inner">
                        <div id="magic-number-container">
                            <div className="s_20"></div>
                            <MagicNumberContent/>
                            <MagicNumberExperience/>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
function mapStateToProps(state){
    return {
        currentLanguage:state.languageDexReducer.CurrentLanguage
    }

}

export default connect(mapStateToProps,{setCurrentPageName})(FavorireNumberPage);