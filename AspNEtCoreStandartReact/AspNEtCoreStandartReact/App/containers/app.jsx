import React from 'react';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { setDexLanguage, setCurrentLanguage, autologinUser } from '../actions/index.jsx';



import CorporateHeader from "../components/header/corporateHeader.jsx";
import PopupManager from "../components/popup/PopupManager.jsx";
import NotifyBox from "../components/header/NotifyBox.jsx";
import PersonalBar from "../components/header/PersonalBar.jsx";
import PageTitle from "../components/header/PageTitle.jsx";
import LocalNavigationBox from "../components/header/LocalNavigationBox.jsx";
import FooterAndLinks from '../components/footer/FooterAndLinks.jsx';
import AlwaisArround from '../components/footer/AlwaysArround.jsx';
import PageToTop from '../components/footer/PageToTop.jsx';

import ResourcePage from './ResourcesPage.jsx';
import DashboarPage from "./DashboardPage.jsx";
import PromoPage from "./PromoPage.jsx";
import OptionsAndServicesPage from "./OptionsAndServicePage.jsx";
import OptionsDeactivatePage from "./OptionsDeactivationsPage.jsx";
import FavoriteNumbersPage from './FavoriteNumbersPage.jsx';
import TopUpPage from './TopUpPage.jsx';
import CreditCardPaymentPage from './CreditPaymentPage.jsx';
import SettingsPage from './SettingsPage.jsx';
import ScratchCardRechargePage from './ScratchCardRechargePage.jsx';
import InvoicePage from './InvoicePage.jsx';
import LoginPage from './LoginPage.jsx';


class  App extends React.Component{

constructor(props){
  super(props);
  this.setCurentLanguage=this.setCurentLanguage.bind(this);
}


    componentDidMount() {
        ///https://third-diagram-179021.firebaseio.com/languageDex.json
        fetch("https://third-diagram-179021.firebaseio.com/languageDex.json").then(res => { return res.json(); }).then(resp => {
        console.log("Response from firebase",resp);
        this.setCurentLanguage(resp,"ro");
        this.props.setDexLanguage(resp);
        });

        //fetch("https://third-diagram-179021.firebaseio.com/User.json").then(res => res.json()).then(resp => {
        //    this.props.autologinUser(resp);
        //    ///console.log("User from firebase", resp);
        //});

        fetch("https://third-diagram-179021.firebaseio.com/languageDex.json").then(res => { return res.json(); }).then(resp => {
            console.log("Response from firebase", resp);
            //this.setCurentLanguage(resp, "ro");
            //this.props.setDexLanguage(resp);
        });

    }
    componentWill

    setCurentLanguage(data){
      const dexCurLan=[];
      var lang=getCookie("GUEST_LANGUAGE_ID");
      console.log("Gueast language :::::::::",lang);
      switch(lang){
        case "ro_RO":
            var result = data.forEach(element => {
              dexCurLan[element.key]=element.textRo;
            });
            console.log("Dictionary Sorted ", dexCurLan);
            this.props.setCurrentLanguage(dexCurLan);
          return;
        case "en_EN":
            var result = data.forEach(element => {
              dexCurLan[element.key]=element.textEn;
            });
            console.log("Dictionary Sorted ", dexCurLan);
            this.props.setCurrentLanguage(dexCurLan);
          return;
        case "ru_RU":
            var result = data.forEach(element => {
              dexCurLan[element.key]=element.textRu;
            });
            console.log("Dictionary Sorted ", dexCurLan);
            this.props.setCurrentLanguage(dexCurLan);
          return;
        default :
            var result = data.forEach(element => {
              dexCurLan[element.key]=element.textRo;
            });
            console.log("Dictionary Sorted ", dexCurLan);
            this.props.setCurrentLanguage(dexCurLan);
          return;
      }

    }

  render(){    
    return (
      <BrowserRouter className="App">
            
            <div>
                  <PopupManager Show={true} />
                  <CorporateHeader/>
                  <NotifyBox/>
                  <PersonalBar />
                  <PageTitle />
                  <LocalNavigationBox />
                <Switch>
                    <Route exact path="/" component={!this.props.User.IsAuthenticated ? LoginPage : DashboarPage} />
                    <Route path="/balance/currentbalance" component={!this.props.User.IsAuthenticated ? LoginPage : ResourcePage}/>
                    <Route path="/promo/promo" component={!this.props.User.IsAuthenticated ? LoginPage : PromoPage} />
                    <Route path="/optionsandservices/optionsservicelist" component={!this.props.User.IsAuthenticated ? LoginPage : OptionsAndServicesPage} />
                    <Route path="/optionsandservices/manageoptionsandservices" component={!this.props.User.IsAuthenticated ? LoginPage : OptionsDeactivatePage} />
                    <Route path="/favoritenumbers/favoritenumbers" component={!this.props.User.IsAuthenticated ? LoginPage : FavoriteNumbersPage} />
                    <Route path="/payment/topup" component={!this.props.User.IsAuthenticated ? LoginPage : TopUpPage}/>
                    <Route path='/payment/creditcartpayment' component={!this.props.User.IsAuthenticated ? LoginPage : CreditCardPaymentPage} />
                    <Route path="/account/accountinfo" component={!this.props.User.IsAuthenticated ? LoginPage : SettingsPage}/>
                    <Route path="/payment/scratchcardrecharge" component={!this.props.User.IsAuthenticated ? LoginPage : ScratchCardRechargePage}/>
                    <Route path="/payment/recharge" component={!this.props.User.IsAuthenticated ? LoginPage : InvoicePage} />

                    <Route path="/login/login" component={LoginPage} />

                    <Route  component={()=>{return <h1>Pagina data NU exista!!</h1>}}/>
                  </Switch>
                  <PageToTop />
                  <AlwaisArround />
                  <FooterAndLinks />
            </div>
              
      </BrowserRouter>
      );
  }
}

function mapStateToProps(state){
  console.log(state);
  return{
      languageDex:state.languageDictionary,
      currentLanguage: state.languageDexReducer,
      User: state.userReducer.User


  }
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


export default connect(mapStateToProps, {autologinUser,setDexLanguage,setCurrentLanguage})(App);



