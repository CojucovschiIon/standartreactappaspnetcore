import React from 'react';



export default class ScratchCardRechargePage extends React.Component{


    someMethod(){
        ///"if(this.checked){ document.getElementById('NewPhoneNumber').focus();}";
    }

    render(){
        return(
            <section id="topup-flow">
            <div className="gridContainer clearfix">
                <div className="s_20"></div>
                
        
        <div className="step-bar-box">
            <div className="step-bar-item done current">
                <a className="stepbar-link" aria-label="true" title="Alege numărul">
                    <span className="step-number">1.</span>
                    <span className="step-label">Alege numărul</span>
                </a>
            </div>
            <div className="step-bar-item ">
                <a className="stepbar-link" aria-label="true" title="Introdu codul de reîncărcare	">
                    <span className="step-number">2.</span>
                    <span className="step-label">Introdu codul de reîncărcare	</span>
                </a>
            </div>
            <div className="step-bar-item ">
                <a className="stepbar-link" aria-label="true" title="Verificare">
                    <span className="step-number">3.</span>
                    <span className="step-label">Verificare</span>
                </a>
            </div>
        </div>
                <div className="s_20 dt"></div>
                <div className="s_20"></div>
                <div className="section-inner">
                    <div>
                        <h5>Alege un număr pentru reîncărcare	</h5>
                    </div>
                    <div className="s_40 "></div>
        <form action="/payment/scratchcardrecharge" className="topup-flow" id="form-recharge" method="post" noValidate="novalidate"> 
                       <div className="flex-container">
                            <div className="w-50">
                                <div className="flex-container">
                                    <div>
                                        <div className="radio-item-branded">
                                            <input type="radio" id="s-option" name="a1"/>
                                            <label htmlFor="s-option">&nbsp;</label>
                                            <div className="check"><div className="inside"></div></div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="inner-item">
                                            <label htmlFor="s-option" data-scope="title">Numărul meu</label>
                                            <div data-scope="sub-title-cta">069197044</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="s_20"></div>
                            </div>
        
                            <div id="newNumberWrapperContent" className="w-50">
                                <div className="flex-container">
                                    <div className="radio-item-branded">
                                        <input type="radio" id="s-option-2" name="a1" />
                                        <label htmlFor="s-option-2">Alt număr	</label>
                                        <div className="check"><div className="inside"></div></div>
                                    </div>
                                </div>
                                <div className="s_10"></div>
        
                                <input aria-describedby="" aria-invalid="false" autoComplete="on" className="numbers-only alt-numar" data-msg="Introdu numărul Orange " data-msg-regx="Formatul numărului introdus este incorect." id="NewPhoneNumber" maxLength="9" name="NewPhoneNumber" placeholder="0XXXXXXXX" type="tel"/>
                                <br/>
                            </div>
                        </div>
                        <div className="flex-container">
                            <div className="w-100">
                                <div className="s_10"></div>
                                <button type="reset">Anulează</button>
                                &nbsp;
                                <button type="submit" htmlFor="form-recharge" className="cta">Continuă</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>
            <div className="s_40"></div>
        </section>
        );
    }
}


