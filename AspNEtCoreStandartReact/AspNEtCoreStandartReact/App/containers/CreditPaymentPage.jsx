import React from 'react';


export default class CreditCardPaymentPage extends React.Component{
    constructor(props){
        super(props);
        this.onCancelClick=this.onCancelClick.bind(this);
        this.whenSoption2Clicked=this.whenSoption2Clicked.bind(this);
        this.whenSoption1Clicked = this.whenSoption1Clicked.bind(this);

        this.onChangeMsisdn = this.onChangeMsisdn.bind(this);
        this.onSubmitHandle = this.onSubmitHandle.bind(this);
    }
    state = {
        msisdn: "",
        validationMessage:""
    }



    onChangeMsisdn(e)
    {
        let msisdn1 = e.target.value;
        const re = /^[0-9]$/;

        if (re.test(msisdn1.slice(-1)))
        {
            msisdn1 = msisdn1;
        }
        else
        {
            msisdn1 = msisdn1.slice(0, -1);
        }

        if (msisdn1.length >= 0) {
            this.setState({
                msisdn: msisdn1
            });
            this.onSubmitHandle(msisdn1);
        }
    }

    onSubmitHandle(msisdn){
        const regx = /^0{0,1}[6-7]{1}[0-9]{7}$/;

        switch (msisdn.length) {
            case 0:
                this.setState({
                    validationMessage: "Introduceti un numar orange!"
                });
                break;
            default:
                this.setState({
                    validationMessage: "NUmarul introdus are un format gresit!"
                });
                break;
        }

        if (!regx.test(msisdn))
        {
            this.setState({
                validationMessage: "NUmarul introdus are un format gresit!"
            });
            return false;
        }
        return true;
    }


    onCancelClick(){
        console.log('Cancela credit card is clicked !!');
        ///"document.location.href=&quot;&quot;"
        window.location.href='/payment/topup?cancellationtoken=brc';
    }
    
    whenSoption2Clicked(el){
        if(el.checked){ document.getElementById('NewPhoneNumber').focus();}
    }

    whenSoption1Clicked(el)
    {


    }
    
    render(){
        return(
            <section id="topup-flow">
                <div className="gridContainer clearfix">
                    <div className="s_20"/>
                    <div className="step-bar-box">
                        <div className="step-bar-item current">
                            <a className="stepbar-link" aria-label="true" title="Alege numărul">
                                <span className="step-number">1.</span>
                                <span className="step-label">Alege numărul</span>
                            </a>
                        </div>
                        <div className="step-bar-item ">
                            <a className="stepbar-link" aria-label="true" title="Introdu suma">
                                <span className="step-number">2.</span>
                                <span className="step-label">Introdu suma</span>
                            </a>
                        </div>
                        <div className="step-bar-item ">
                            <a className="stepbar-link" aria-label="true" title="Verificare">
                                <span className="step-number">3.</span>
                                <span className="step-label">Verificare</span>
                            </a>
                        </div>
                        <div className="step-bar-item ">
                            <a className="stepbar-link" aria-label="true" title="Achitare">
                                <span className="step-number">4.</span>
                                <span className="step-label">Achitare</span>
                            </a>
                        </div>
                    </div>
                    <div className="s_20 dt"/>

                    <section id="notify-box">
                        <div className="gridContainer clearfix">
                            <div className="flex-container">

                                <div className="dt" style={{marginRight:'15px'}}>
                                    <img src="/Content/assets/img/bonus-ico.gif" className="icon" style={{height:'70px',width:'70px'}} alt="bonus-ico"/>
                                </div>

                                <div>
                                    <div className="inner-item">
                                        <div data-scope="title">
                                            Acum, fiecare reîncărcare online a contului Orange de minim 100 lei vă aduce un bonus de 1 GB trafic Internet, disponibil 24 ore. Bonusul se va aloca automat, la finalizarea cu succes a tranzacției, pe numărul indicat în procesul de reîncărcare.
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </section>

                    <div className="s_20"/>

                    <section className="tab-item">
                        <div>
                            <h5>Alege un număr pentru reîncărcare	</h5>
                        </div>
                        <div className="s_40"/>
                    <form action="/payment/creditcartpayment" className="topup-flow" id="form-recharge" method="post" novalidate="novalidate">                
                            <div className="flex-container">
                                <div className="w-50">
                                    <div className="flex-container">
                                        <div>
                                            <div className="radio-item-branded">
                                                <input type="radio" id="s-option" name="a1" defaultChecked={true} onClick={this.whenSoption1Clicked}/>
                                                <label htmlFor="s-option">&nbsp;</label>
                                                <div className="check"><div class="inside"></div></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div className="inner-item">
                                                <label for="s-option" data-scope="title">Numărul meu</label>
                                                <div data-scope="sub-title-cta">069197044</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="s_20"></div>
                                </div>
                                <div id="newNumberWrapperContent" class="w-50">
                                    <div className="flex-container">
                                        <div className="radio-item-branded">
                                            <input type="radio" id="s-option-2" name="a1" onClick={this.whenSoption2Clicked}/>
                                            <label for="s-option-2">Alt număr	</label>
                                            <div className="check"><div className="inside"></div></div>
                                        </div>
                                    </div>
                                    <div className="s_10"></div>
                                    <input aria-describedby=""
                                        aria-invalid="false"
                                        onChange={this.onChangeMsisdn}
                                        value={this.state.msisdn}
                                        autocomplete="on"
                                        className="numbers-only alt-numar"
                                        data-msg="Introdu numărul Orange "
                                        data-msg-regx="Formatul numărului introdus este incorect."
                                        id="NewPhoneNumber"
                                        maxlength="9"
                                        name="NewPhoneNumber"
                                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                        placeholder="0XXXXXXXX"
                                        type="tel" />
                                    <br/>

                                </div>
                            </div>
                            <div className="flex-container">
                                <div className="w-100">
                                    <div className="s_10"></div>
                                    <button type="reset" onClick={this.onCancelClick}>Anulează</button>
                                    &nbsp;
                                    <button type="submit" className="cta" form="form-recharge">Continuă</button>
                                </div>
                            </div>
                            <input name="__RequestVerificationToken" type="hidden" value="CfDJ8GQuFo_LNjpHimqGrkFYbjLoHYkgaDTa_dgtmoyoFixqlu2H9aTo6ZWrqImb1GDvYFVfkOYRgBM_I0RxvXFjJ4j90LEqDgI3uuhG2kGCk1GkSkoOX0s6YAC7BuokBYHUoAIBGvMpJRl5NUx_FJEPoeY"/>
                    </form>
                    </section>
                </div>
            </section>
        );
    }
}

