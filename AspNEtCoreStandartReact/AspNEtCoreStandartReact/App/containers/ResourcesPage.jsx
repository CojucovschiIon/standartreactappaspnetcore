import React from "react";
import NotifyBox from "../components/resourcesPage/NotifyBox.jsx"
import ResourcesContainer from "../components/resourcesPage/ResoucesContainer.jsx"

export default class ResourcesPage extends React.Component{


    render(){
        return(
            <div id="resources-page">
                <NotifyBox />
                <ResourcesContainer />
            </div>
        );
    }
}
