import React from 'react';
import { Link } from 'react-router-dom';


export default class TopUpPage extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <section id="topup-flow">
                <div className="gridContainer clearfix">
                    <div className="section-inner">

                        <div className="s_40"></div>
                        <div>
                            <h5>Alege modalitatea de plată</h5>
                        </div>
                        <div className="s_40 "></div>

                        <div className="flow-selector-items flex-container nowrap valign-stretch">
                            <div className="selector-item w-25">
                                <div>
                                    <div className="img-holder">
                                        <img src="/Content/assets/img/topup-o-a.png" alt="top up" />
                                    </div>
                                    <span className="s_20"></span>
                                </div>

                                <div>
                                    <strong>Cu cardul bancar</strong>
                                    <span className="s_20"></span>
                                </div>

                                <div>
                                    <Link to="/payment/creditcartpayment" className='button' >Alege</Link>
                                    <span className="s_20"></span>
                                </div>
                            </div>
                            <div className="selector-item w-25">
                                <div>
                                    <div className="img-holder">
                                        <img src="/Content/assets/img/topup-o-b.png" alt="top up" />
                                    </div>
                                    <span className="s_20"></span>
                                </div>

                                <div>
                                    <strong>Cu cartela de reîncărcare</strong>
                                    <span className="s_20"></span>
                                </div>

                                <div>
                                    <Link to="/payment/scratchcardrecharge" className='button'>Alege</Link>
                                    <span className="s_20"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="s_40"></div>
            </section>
        );
    }
}

