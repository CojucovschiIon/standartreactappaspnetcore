import React from 'react';
import {Link } from 'react-router-dom';

import OptionDeactivate from '../components/optionsAndServices/OptionDeactivateForm.jsx';

export default class OptionsDeactivationPage extends React.Component{
    render(){
        return(
            <section id="options-n-services">
                <div class="m">
                    <ul class="tab-c">
                        <li onclick="document.location.href='/optionsandservices/optionsservicelist'">
                            <span>Activează</span>
                        </li>
                        <li class="active" onclick="document.location.href='/optionsandservices/manageoptionsandservices'">
                            <span>Dezactivează</span>
                        </li>
                    </ul>
                </div>
                <div class="gridContainer clearfix">
                    <div class="dt">
                        <span class="s_20"></span>
                        <ul class="tab-b">
                            <li>
                                <span><Link to="/optionsandservices/optionsservicelist"><span>Activează</span></Link></span>
                            </li>
                            <li class="active">
                                <span><Link to="/optionsandservices/manageoptionsandservices">Dezactivează</Link></span>
                            </li>
                        </ul>
                        <span class="s_10"></span>
                    </div>
                    <ul class="list">
                        <li class="list-item type-a">
                            <OptionDeactivate />   
                        </li>
                        <li class="list-item type-a">
                            <OptionDeactivate />   
                        </li>
                        <li class="list-item type-a">
                            <OptionDeactivate />   
                        </li>
                    </ul>
                </div>
            </section>
        );
    }
}

