import React from 'react';


export default class SettingsPage extends React.Component{
    render(){
        return(
            <div className="last-animation contentEffect" style={{display: 'block'}}>
                <section id="account-page">
                    <div className="gridContainer clearfix m-no-padding">
                        <div className="gridContainer clearfix">
                            <div className="accordion-container">
                                <div className="accordion-item">

                                    <div className="accordion-header flex-container">
                                        <span className="icon icon-my-account"></span>
                                        <span>Informaţii personale</span>
                                    </div>

                                    <div className="accordion-contents" style={{display: 'none'}}>
                                        <div className="flex-container">
                                            <div>
                                                <div className="inner-item">
                                                    <div data-scope="title">Limba de comunicare cu Orange</div>
                                                    <div data-scope="sub-title-cta">Română</div>
                                                    


                <div data-scope="modify-accordion-box">
                    <button className="clean no-padding button-modify " data-scope="open-edit-form">
                        <span className="icon icon-Pencil"></span> <u>Modifică</u>
                    </button>
                    <div data-scope="modify-form" style={{display: 'none'}}>
                        <select id="languageModValue" className="styled w-border" name="DefaultLangText">
                            <option selected="selected" value="ro">Română</option>
                            <option value="ru">Русский</option>
                            <option value="en">English</option>
                        </select>
                        <div className="s_10"></div>
                        <button id="cancelModification" data-scope="open-edit-form-trigger">Anulează</button>
                        &nbsp;
                        <button onclick="modifyLanguagefunction()" id="modLang" className="cta" type="submit">Activează</button>

                    </div>
                </div>
                                                </div>
                                            </div>
                                            <div className="inner-item">
                                                <div data-scope="title">Adresa de email</div>
                                                    <div data-scope="sub-title-cta">ion.cojucovschi@orange.md</div>

                                                    

                            <div data-scope="modify-accordion-box">
                                <button className="clean no-padding button-modify " data-scope="open-edit-form">
                                    <span className="icon icon-Pencil"></span> <u>Modifică</u>
                                </button>

                                <div data-scope="modify-form" style={{display: 'none'}}>
                                    <input style={{width:'80%'}} type="email" id="emailValueParam" placeholder="Introdu adresa ta de email"/>
                                    <div className="s_10"></div>
                                    <button id="cancelModification" data-scope="open-edit-form-trigger">Anulează</button>
                                    &nbsp;
                                    <button onclick="userEmailActivation()" id="modLang" className="cta" type="submit">Activează</button>
                                </div>
                            </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="accordion-item">

                                    <div className="accordion-header flex-container">
                                        <span className="icon icon-simple-mode"></span>
                                        <span>Informaţii despre număr</span>
                                    </div>
                                    <div className=" accordion-contents" style={{display: 'none'}}>
                                        <div className="flex-container">

                                            <div>
                                                <div className="inner-item">
                                                    <div data-scope="title">PIN1 iniţial</div>
                                                    <div data-scope="sub-title-cta">1111</div>
                                                </div>
                                            </div>

                                            <div>
                                                <div className="inner-item">
                                                    <div data-scope="title">PUK1</div>
                                                    <div data-scope="sub-title-cta">28236472</div>
                                                </div>
                                            </div>

                                            <div>
                                                <div className="inner-item">
                                                    <div data-scope="title">PIN2 iniţial</div>
                                                    <div data-scope="sub-title-cta">2845</div>
                                                </div>
                                            </div>

                                            <div>
                                                <div className="inner-item">
                                                    <div data-scope="title">PUK2</div>
                                                    <div data-scope="sub-title-cta">14141434</div>
                                                </div>
                                            </div>

                                            <div>
                                                <div className="inner-item">
                                                    <div data-scope="title">Consum în afara abonamentului pe număr în luna curentă</div>
                                                    <div data-scope="sub-title-cta">0 MDL</div>
                                                </div>
                                            </div>

                                            <div>
                                                <div className="inner-item">
                                                    <div data-scope="title">Data activării numărului la Orange Abonament</div>
                                                        <div data-scope="sub-title-cta">
                                                            22.11.2018
                                                        </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div id="activate-invoice-by-mail-block" className="accordion-item">

                                    <div className="accordion-header flex-container">
                                        <span className="icon icon-document-stack"></span>
                                        <span>Servicii factura</span>
                                    </div>

                                    <div className="accordion-contents" style={{display: 'none'}}>
                                        <div className="flex-container">
                                            <div className="inner-item ">
                                                <div data-scope="title">Factura lunară</div>
                                                <div data-scope="sub-title-cta">Prin email</div>
                                                <div className="s_10"></div>
                                                <div data-scope="modify-accordion-box">
                                                    <div data-scope="modify-form" className="active" style={{display: 'block'}}>
                                                                <div data-scope="modify-form" style={{display: 'initial'}}>

                                                                    <button data-account="#" data-account-tmpl="account_activateDeactivate" data-deactivate_invoice_service="True" data-action="deactivate" className="cta" data-confirmation_text="Dezactivează" type="submit" data-scope="open-edit-form-trigger">
                                                                        Dezactivează
                                                                    </button>
                                                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="accordion-item">

                                    <div className="accordion-header flex-container" id="#fromPromo">
                                        <span class="icon icon-info"></span>
                                        <span>Informaţii despre cont</span>
                                    </div>

                                    <div className="accordion-contents" style={{display: 'none'}}>
                                        <div className="flex-container">
                                            <div>
                                                <div className="inner-item">
                                                    <div data-scope="title">Numărul contului</div>
                                                    <div data-scope="sub-title-cta">6391333</div>
                                                </div>
                                            </div>

                                            <div>
                                                <div className="inner-item">
                                                    <div data-scope="title">Consum în afara abonamentului pe cont pentru luna curentă</div>
                                                    <div data-scope="sub-title-cta">0 MDL</div>
                                                </div>
                                            </div>

                                            <div>
                                                <div className="inner-item">
                                                    <div data-scope="title">Limita de Credit pe cont</div>
                                                        <div data-scope="sub-title-cta">0 MDL</div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div style={{display:'none'}}>
                    <form className="ons-form-item" id="account_activateDeactivate" novalidate="novalidate">

                        <h2 className="data-option-action">Sigur doreşti dezactivarea serviciului?</h2>
                        <span className="s_10 dialog-description"></span>
                        <h3 className="data-option-description"></h3>

                        <span className="s_20"></span>
                        <ul className="tab-d theme-a">
                            <li>
                                <h3 className=".optin-title data-option-name"></h3>
                            </li>
                            <li>
                                <h3 className="txt-color-cta data-option-price"></h3>
                            </li>
                        </ul>
                        <span className="s_20"></span>
                        <ul className="form-cta-holder">
                            <li>
                                <div data-popup-role="close" className="inline button">Anulează</div>
                                &nbsp;&nbsp;
                                <div className="inline button cta" id="btn-account-deactivate" data-option-form-id="" data-option-id="">Dezactivează</div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        );
    }
}


