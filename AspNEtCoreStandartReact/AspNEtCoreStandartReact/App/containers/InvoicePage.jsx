import React from 'react';

import LastInvoiceComponent from '../components/invoice/postpay/LastInvoice.jsx';



export default class InvoicePage extends React.Component{

    render(){
        return(
            <section id="invoices">
                <input id="allmonthsStringArray" type="hidden" value="[&quot;Iunie&quot;,&quot;Iulie&quot;,&quot;August&quot;,&quot;Septembrie&quot;,&quot;Octombrie&quot;,&quot;Noiembrie&quot;]"/>
                <input id="allmonthsBillIncluded" type="hidden" value="[0.0,0.0,0.0,680.0,0.0,1710.0]" />
                <input id="allmonthsExtraOptionsArray" type="hidden" value="[0.0,0.0,0.0,0.0,0.0,456.99]" />
                  <div className="gridContainer clearfix">
                    <div className="section-inner">
                        <LastInvoiceComponent />

                    </div>
                </div>
            </section>
        );
    }



}




