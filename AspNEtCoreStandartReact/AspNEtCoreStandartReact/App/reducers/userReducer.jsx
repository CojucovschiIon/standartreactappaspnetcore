﻿const userDefault = {
    User: {
        FullMsisdn: "",
        Ip: "192.168.33.14",
        IsAuthenticated: false,
        Msisdn: "",
        UserType: "UNLOGED"
    }
};

export default (state = userDefault, action) => {

    switch (action.type) {
        case "AUTO_LOGIN":
            console.log("Language was changed");
            return { ...state, User: action.payload };
        case "OTP_LOGIN":

            return { ...state, User: action.payload };
        default:
            return state;
    }
}
