var pageName={PageName:"Acasă"};

export default (state=pageName, action)=>{

    switch(action.type){
        case "CHANGE_PAGE_NAME":
            return { ...state,PageName :action.payload}
        default : 
            return state;
        }
}
