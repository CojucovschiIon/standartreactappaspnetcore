const CurrentLanguage={CurrentLanguage:[]};

export default (state=CurrentLanguage, action)=>{

    switch(action.type){
        case "CHANGE_LANG":
            console.log("Language was changed");
            return {...state,CurrentLanguage:action.payload}
        default : 
            return state;
        }
}
