import { combineReducers } from 'redux';

import languageDexReducer from './languageReducer.jsx';
import languageDictionary from './languageDictionary.jsx';
import popupReducer from './popupReducer.jsx';
import pageNameReducer from './pageNameReducer.jsx';
import userReducer from './userReducer.jsx';

export default combineReducers({languageDexReducer,
     languageDictionary,
     popupReducer,
    pageNameReducer,
    userReducer
    });
