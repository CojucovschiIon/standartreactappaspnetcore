var popupModel={PopupModel:{Show:false}};

export default (state=popupModel, action)=>{

    switch(action.type){
        case "POPUP_OPTION_SERVICE":
            console.log("reducer pop up ",state);
            return { ...state,PopupModel :{Show:true,Template:"option_activate",PostUrl:"OPtionsAndService/activate"}}
        case "CLOSE_POPUP":
            console.log("reducer pop up ",state);
            return { ...state,PopupModel :{Show:false}}
        case "POPUP_RESPONSE_MESSAGE":
            return { ...state,PopupModel :{Show:true,Template:"response_mesage",PostUrl:"OPtionsAndService/activate"}}

        default : 
            return state;
        }
}
