export const setCurrentLanguage=(date)=>{
    return{
        type:"CHANGE_LANG",
        payload:date
    }
}

export const setDexLanguage=(date)=>{
    return{
        type:"SET_DICTIONARY",
        payload:date
    }
}
