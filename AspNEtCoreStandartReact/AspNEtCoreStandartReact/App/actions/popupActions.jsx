export const showActivationOptionPopup=(date)=>{
    return{
        type:"POPUP_OPTION_SERVICE",
        payload:date
    }
}
export const closePopup=(date)=>{
    return{
        type:"CLOSE_POPUP",
        payload:date
    }
}

///Response popup 
export const showResponseMessagePopup=(date)=>{
    console.log("ShowResponse Pop up ");
    return{
        type:"POPUP_RESPONSE_MESSAGE",
        payload:date
    }
}