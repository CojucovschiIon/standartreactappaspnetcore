﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AspNEtCoreStandartReact.Controller
{
    public class MainController:BaseController
    {
        public JsonResult GetTextResult()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync("https://third-diagram-179021.firebaseio.com/languageDex.json").Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            List<TextResource> textRes = JsonConvert.DeserializeObject<List<TextResource>>(responseBody);
            return Json(textRes);
        }

    }
    public class TextResource
    {
        public int id { get; set; }
        public string key { get; set; }
        public string textEn { get; set; }
        public string textRo { get; set; }
        public string textRu { get; set; }
    }

}
