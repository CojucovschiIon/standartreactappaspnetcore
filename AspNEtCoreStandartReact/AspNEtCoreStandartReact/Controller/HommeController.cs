﻿using AspNEtCoreStandartReact.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNEtCoreStandartReact.Model;

namespace AspNEtCoreStandartReact.Controller
{
    public class HommeController : Microsoft.AspNetCore.Mvc.Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GetOPtions()
        {
            List<Option> model = new List<Option>();
            string[] categories = { "Category1", "Category2", "Category 3", "Category 5", "Si incao categorie" };
            string description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
            Random random = new Random();
            for (int i = 0; i < 100; i++)
            {
                model.Add(new Option { Id = "some_id" + i, Category = categories[random.Next(4)] });
            }
            return Ok(model);
        }
        public IActionResult GetTours()
        {
            string[] city = { "San Francisco", "Paris", "Miami", "Rio" };
            string[] images = { "/img/paris.jpg", "/img/sanfran.jpg", "/img/newyork.jpg" };
            List<object> resp = new List<object>();
            Random random = new Random();
            for (int i = 0; i < 100; i++)
            {
                resp.Add(new {
                    key = i,
                    name = city[random.Next(4)],
                    image = images[random.Next(2)],
                    date = DateTime.Now.ToString("dd/mm/yyyy")
                });
            }
            return Ok(resp);
        }
        public IActionResult GetTextResources(string lang="ro")
        {
            List<object> list = new List<object>();
            for (int i = 0; i < 600; i++)
            {
                switch (lang)
                {
                    case "ro":
                        list.Add(new { key = "key.val" + i, value = "RomanianText" + i });
                        break;
                    case "ru":
                        list.Add(new { key = "key.val" + i, value = "RusianText" + i });
                        break;
                    case "en":
                        list.Add(new { key = "key.val" + i, value = "EnglishText" + i });
                        break;
                    default:
                        list.Add(new { key = "key.val" + i, value = "RomanianText" + i });
                        break;
                }
            }
            return Ok(list);
        }

        public IActionResult AutloLogin()
        {
            User user = new User();
            user.Msisdn = "60031400";
            user.Name = "ION";
            user.UserType = "B2C";
            user.UserStatusType = "Active";
            return Ok(user);
        }

        public IActionResult AutologinSimulation(bool SimulateOrNot=false)
        {
            User user = new User();
            if (SimulateOrNot)
            {
                user.IsAuthenticated = true;
                user.Msisdn = "69197044";
                user.UserStatusType = "Active";
                user.UserType = "B2C";
            }
            else
            {
                user.IsAuthenticated = false;
            }
            return Ok(user);

        }

    }
}
