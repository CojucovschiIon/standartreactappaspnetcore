﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNEtCoreStandartReact.Model
{
    public class Option
    {
        public string Category { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
    }
}
