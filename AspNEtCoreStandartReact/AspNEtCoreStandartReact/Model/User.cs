﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNEtCoreStandartReact.Model
{
    public class User
    {
        public string Msisdn { get; set; }
        public string Name { get; set; }
        public string UserType { get; set; }
        public string UserStatusType { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}
